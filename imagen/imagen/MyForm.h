#pragma once

namespace imagen {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected:
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(44, 44);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(422, 259);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &MyForm::pictureBox1_Click);
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(512, 44);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(401, 259);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 1;
			this->pictureBox2->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(320, 366);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(103, 36);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Escala de grises";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(477, 366);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(93, 36);
			this->button2->TabIndex = 3;
			this->button2->Text = L"Umbral";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(951, 454);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);

		}
	private:
		Bitmap^ image1;
#pragma endregion


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		try
		{

			image1 = gcnew Bitmap(pictureBox1->Image);//"C:\\Users\\Alberto\\Pictures\\monta�a.png", true);

			int x;
			int y;
			for (x = 0; x < image1->Width; x++)
			{
				for (y = 0; y < image1->Height; y++)
				{

					Color pixelColor = image1->GetPixel(x, y);
					int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
					Color newColor = Color::FromArgb(valor, valor, valor);
					image1->SetPixel(x, y, newColor);


				}

			}

			pictureBox2->Image = image1;

		}
		catch (ArgumentException^)
		{
			MessageBox::Show("Error al cargar la imagen");
		}
	}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	Bitmap^ image1;
	Color newColor;
	try
	{

		image1 = gcnew Bitmap(pictureBox2->Image);//"C:\\Users\\Alberto\\Pictures\\monta�a.png", true);

		int x;
		int y;
		for (x = 0; x < image1->Width; x++)
		{
			for (y = 0; y < image1->Height; y++)
			{

				Color pixelColor = image1->GetPixel(x, y);
				
				int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
				if (valor >= 0 && valor <= 50)
				{
					newColor = Color::FromArgb(0, 0, 0);
				}
				else
				{
					 newColor = Color::FromArgb(255,255,255);
				}
				
				image1->SetPixel(x, y, newColor);


			}

		}

		pictureBox2->Image = image1;

	}
	catch (ArgumentException^)
	{
		MessageBox::Show("Error al cargar la imagen");
	}
}
private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {

}
};
}
