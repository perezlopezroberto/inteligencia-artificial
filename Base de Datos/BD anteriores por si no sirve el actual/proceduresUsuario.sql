USE `estacionamiento`;
DROP procedure IF EXISTS `insertaUsuario`;

DELIMITER $$
USE `estacionamiento`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertaUsuario`(in matri varchar(10),
in nomb varchar(45),in apat varchar(45),in amater varchar(45),in direc varchar(100),
in tel varchar(10),in iarea varchar(45))
BEGIN
insert into usuario values(matri,nomb,apat,amater,direc,
tel,iarea);
END$$

DELIMITER ;

USE `estacionamiento`;
DROP procedure IF EXISTS `actualizaUsuario`;

DELIMITER $$
USE `estacionamiento`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizaUsuario`(in matric varchar(10),
in nomb varchar(45),in apat varchar(45),in amater varchar(45),in direc varchar(100),
in tel varchar(10),in iarea varchar(45))
BEGIN
update usuario set matricula=matri,nombre=nomb,apaterno=apat,amaterno=amater,direccion=direc,
telefono=tel,area=iarea where matricula=matric;
END$$

DELIMITER ;

USE `estacionamiento`;
DROP procedure IF EXISTS `consultaUsuarios`;

DELIMITER $$
USE `estacionamiento`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaUsuarios`()
BEGIN
select * from usuario;
END$$

DELIMITER ;
-- Busca Usuarios Por ID
USE `estacionamiento`;
DROP procedure IF EXISTS `buscaUsuarioID`;

DELIMITER $$
USE `estacionamiento`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaUsuarioID`(in iID varchar(45))
BEGIN
select * from usuario WHERE matricula=iID;
END$$
DELIMITER ;

-- Busca Usuarios Por Nombre
USE `estacionamiento`;
DROP procedure IF EXISTS `buscaUsuarioNombre`;

DELIMITER $$
USE `estacionamiento`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaUsuarioNombre`(in inombre varchar(45))
BEGIN
select * from usuario WHERE nombre like CONCAT('%', inombre, '%');
END$$
DELIMITER ;

-- Busca Usuarios Por Apellido Paterno
USE `estacionamiento`;
DROP procedure IF EXISTS `buscaUsuarioApPaterno`;

DELIMITER $$
USE `estacionamiento`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaUsuarioApPaterno`(in iApPaterno varchar(45))
BEGIN
select * from usuario WHERE apaterno like CONCAT('%', iApPaterno, '%');
END$$
DELIMITER ;
