CREATE SCHEMA estacionamiento;
SET search_path TO estacionamiento;
CREATE TABLE usuario
(
matricula  VARCHAR(10) CONSTRAINT pkey PRIMARY KEY,
nombre VARCHAR(30),
apaterno VARCHAR(30),
amaterno VARCHAR(30),
direccion VARCHAR(100),
telefono VARCHAR(10),
area VARCHAR(50)
);

ALTER TABLE usuario
 OWNER TO postgres;

CREATE TABLE vehiculo
(
id INTEGER  CONSTRAINT pky PRIMARY KEY,
placa VARCHAR(10),
marca VARCHAR(30),
color VARCHAR(20),
tipo VARCHAR(30),
matricula VARCHAR(10),
foreign key (matricula)
references usuario (matricula)
);

ALTER TABLE vehiculo
OWNER TO postgres;

CREATE TABLE Registro(
id_reporte INTEGER CONSTRAINT llaveprimaria PRIMARY KEY,
tiempoTotal VARCHAR(20)
);
ALTER TABLE Registro
OWNER TO postgres;


CREATE TABLE ReporteVehicular(
id_vehiculo INTEGER,
horaEntrada TIMESTAMP, 
horaSalida TIMESTAMP,
id_reporte INTEGER,
foreign key (id_vehiculo)
references vehiculo (id),
foreign key (id_reporte)
references  Registro (id_reporte)
);
ALTER TABLE ReporteVehicular
OWNER TO postgres;

CREATE TABLE EntradaPeaton(
folio INTEGER CONSTRAINT llaveprim PRIMARY KEY,
nombre VARCHAR(30),
apaterno VARCHAR(30),
amaterno VARCHAR(30),
horaEntrada TIMESTAMP, 
horaSalida TIMESTAMP,
motivoVisita VARCHAR(100),
IDreporte INTEGER,
foreign key (IDreporte)
references Registro (id_reporte)
);