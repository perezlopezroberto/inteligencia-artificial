#pragma once
#include "Conexion.h"
namespace Formulario {
     
     using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
     using namespace MySql::Data::MySqlClient;     
	/// <summary>
	/// Resumen de Usuario
	/// </summary>
	public ref class Usuario : public System::Windows::Forms::Form
	{
	public:
		Usuario(MySql::Data::MySqlClient::MySqlConnection^ conn)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
			this->conn = conn;
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Usuario()
		{
			if (components)
			{
				delete components;
			}
		}


     protected:

    

















     private: System::Windows::Forms::Button^  cancelBTN;
     private: System::Windows::Forms::DataGridView^  dataGridUsuarios;
     private: System::Windows::Forms::ComboBox^  tipoBusquedaCOMBOBOX;
     private: System::Windows::Forms::Button^  buscarBTN;
     private: System::Windows::Forms::TextBox^  buscarTextBox;
     private: System::Windows::Forms::Label^  label8;
     private: System::Windows::Forms::Label^  label1;
     private: System::Windows::Forms::Button^  agregarBTN;
     private: System::Windows::Forms::TextBox^  nombreText;
     private: System::Windows::Forms::Label^  label2;
     private: System::Windows::Forms::TextBox^  noControlText;
     private: System::Windows::Forms::TextBox^  apPatText;
     private: System::Windows::Forms::TextBox^  apMatText;
     private: System::Windows::Forms::Label^  label3;
     private: System::Windows::Forms::Label^  label4;
     private: System::Windows::Forms::Label^  label5;
     private: System::Windows::Forms::TextBox^  direccionText;
     private: System::Windows::Forms::TextBox^  telefonoText;
     private: System::Windows::Forms::Label^  label6;
     private: System::Windows::Forms::Label^  label7;

     private: System::Windows::Forms::Button^  modificarBTN;

     private: System::Windows::Forms::ComboBox^  estadoCombo;
     private: System::Windows::Forms::Button^  borrarBTN;
     private: System::Windows::Forms::Button^  mostartBTN;
	 private: MySql::Data::MySqlClient::MySqlConnection^ conn;





































	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
              this->cancelBTN = (gcnew System::Windows::Forms::Button());
              this->dataGridUsuarios = (gcnew System::Windows::Forms::DataGridView());
              this->tipoBusquedaCOMBOBOX = (gcnew System::Windows::Forms::ComboBox());
              this->buscarBTN = (gcnew System::Windows::Forms::Button());
              this->buscarTextBox = (gcnew System::Windows::Forms::TextBox());
              this->label8 = (gcnew System::Windows::Forms::Label());
              this->label1 = (gcnew System::Windows::Forms::Label());
              this->agregarBTN = (gcnew System::Windows::Forms::Button());
              this->nombreText = (gcnew System::Windows::Forms::TextBox());
              this->label2 = (gcnew System::Windows::Forms::Label());
              this->noControlText = (gcnew System::Windows::Forms::TextBox());
              this->apPatText = (gcnew System::Windows::Forms::TextBox());
              this->apMatText = (gcnew System::Windows::Forms::TextBox());
              this->label3 = (gcnew System::Windows::Forms::Label());
              this->label4 = (gcnew System::Windows::Forms::Label());
              this->label5 = (gcnew System::Windows::Forms::Label());
              this->direccionText = (gcnew System::Windows::Forms::TextBox());
              this->telefonoText = (gcnew System::Windows::Forms::TextBox());
              this->label6 = (gcnew System::Windows::Forms::Label());
              this->label7 = (gcnew System::Windows::Forms::Label());
              this->modificarBTN = (gcnew System::Windows::Forms::Button());
              this->estadoCombo = (gcnew System::Windows::Forms::ComboBox());
              this->borrarBTN = (gcnew System::Windows::Forms::Button());
              this->mostartBTN = (gcnew System::Windows::Forms::Button());
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridUsuarios))->BeginInit();
              this->SuspendLayout();
              // 
              // cancelBTN
              // 
              this->cancelBTN->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->cancelBTN->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->cancelBTN->Location = System::Drawing::Point(987, 378);
              this->cancelBTN->Name = L"cancelBTN";
              this->cancelBTN->Size = System::Drawing::Size(75, 23);
              this->cancelBTN->TabIndex = 16;
              this->cancelBTN->Text = L"Cancelar";
              this->cancelBTN->UseVisualStyleBackColor = true;
              this->cancelBTN->Click += gcnew System::EventHandler(this, &Usuario::cancelBTN_Click);
              // 
              // dataGridUsuarios
              // 
              this->dataGridUsuarios->AllowUserToAddRows = false;
              this->dataGridUsuarios->AllowUserToDeleteRows = false;
              this->dataGridUsuarios->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
                  | System::Windows::Forms::AnchorStyles::Left)
                  | System::Windows::Forms::AnchorStyles::Right));
              this->dataGridUsuarios->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
              this->dataGridUsuarios->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
              this->dataGridUsuarios->Location = System::Drawing::Point(342, 50);
              this->dataGridUsuarios->Name = L"dataGridUsuarios";
              this->dataGridUsuarios->ReadOnly = true;
              this->dataGridUsuarios->Size = System::Drawing::Size(702, 294);
              this->dataGridUsuarios->TabIndex = 17;
              this->dataGridUsuarios->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Usuario::dataGridUsuarios_CellClick);
              // 
              // tipoBusquedaCOMBOBOX
              // 
              this->tipoBusquedaCOMBOBOX->Cursor = System::Windows::Forms::Cursors::AppStarting;
              this->tipoBusquedaCOMBOBOX->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
              this->tipoBusquedaCOMBOBOX->FormattingEnabled = true;
              this->tipoBusquedaCOMBOBOX->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"ID", L"Nombre", L"Apellido Paterno" });
              this->tipoBusquedaCOMBOBOX->Location = System::Drawing::Point(406, 23);
              this->tipoBusquedaCOMBOBOX->Name = L"tipoBusquedaCOMBOBOX";
              this->tipoBusquedaCOMBOBOX->Size = System::Drawing::Size(121, 21);
              this->tipoBusquedaCOMBOBOX->TabIndex = 21;
              // 
              // buscarBTN
              // 
              this->buscarBTN->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->buscarBTN->Location = System::Drawing::Point(678, 23);
              this->buscarBTN->Name = L"buscarBTN";
              this->buscarBTN->Size = System::Drawing::Size(75, 23);
              this->buscarBTN->TabIndex = 22;
              this->buscarBTN->Text = L"Buscar";
              this->buscarBTN->UseVisualStyleBackColor = true;
              this->buscarBTN->Click += gcnew System::EventHandler(this, &Usuario::buscarBTN_Click);
              // 
              // buscarTextBox
              // 
              this->buscarTextBox->Location = System::Drawing::Point(533, 23);
              this->buscarTextBox->Name = L"buscarTextBox";
              this->buscarTextBox->Size = System::Drawing::Size(139, 20);
              this->buscarTextBox->TabIndex = 23;
              // 
              // label8
              // 
              this->label8->AutoSize = true;
              this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label8->Location = System::Drawing::Point(339, 26);
              this->label8->Name = L"label8";
              this->label8->Size = System::Drawing::Size(72, 13);
              this->label8->TabIndex = 20;
              this->label8->Text = L"Buscar por:";
              // 
              // label1
              // 
              this->label1->AutoSize = true;
              this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label1->Location = System::Drawing::Point(12, 79);
              this->label1->Name = L"label1";
              this->label1->Size = System::Drawing::Size(54, 13);
              this->label1->TabIndex = 0;
              this->label1->Text = L"Nombre:";
              // 
              // agregarBTN
              // 
              this->agregarBTN->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->agregarBTN->Location = System::Drawing::Point(23, 271);
              this->agregarBTN->Name = L"agregarBTN";
              this->agregarBTN->Size = System::Drawing::Size(75, 23);
              this->agregarBTN->TabIndex = 1;
              this->agregarBTN->Text = L"Agregar";
              this->agregarBTN->UseVisualStyleBackColor = true;
              this->agregarBTN->Click += gcnew System::EventHandler(this, &Usuario::agregarBTN_Click);
              // 
              // nombreText
              // 
              this->nombreText->Location = System::Drawing::Point(117, 76);
              this->nombreText->Name = L"nombreText";
              this->nombreText->Size = System::Drawing::Size(197, 20);
              this->nombreText->TabIndex = 5;
              this->nombreText->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Usuario::nombreText_KeyPress);
              // 
              // label2
              // 
              this->label2->AutoSize = true;
              this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label2->Location = System::Drawing::Point(12, 47);
              this->label2->Name = L"label2";
              this->label2->Size = System::Drawing::Size(59, 13);
              this->label2->TabIndex = 3;
              this->label2->Text = L"Matricula";
              // 
              // noControlText
              // 
              this->noControlText->Location = System::Drawing::Point(117, 47);
              this->noControlText->MaxLength = 8;
              this->noControlText->Name = L"noControlText";
              this->noControlText->Size = System::Drawing::Size(197, 20);
              this->noControlText->TabIndex = 4;
              this->noControlText->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Usuario::noControlText_KeyPress);
              // 
              // apPatText
              // 
              this->apPatText->Location = System::Drawing::Point(117, 102);
              this->apPatText->Name = L"apPatText";
              this->apPatText->Size = System::Drawing::Size(197, 20);
              this->apPatText->TabIndex = 6;
              this->apPatText->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Usuario::apPatText_KeyPress);
              // 
              // apMatText
              // 
              this->apMatText->Location = System::Drawing::Point(117, 128);
              this->apMatText->Name = L"apMatText";
              this->apMatText->Size = System::Drawing::Size(197, 20);
              this->apMatText->TabIndex = 7;
              this->apMatText->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Usuario::apMatText_KeyPress);
              // 
              // label3
              // 
              this->label3->AutoSize = true;
              this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label3->Location = System::Drawing::Point(12, 109);
              this->label3->Name = L"label3";
              this->label3->Size = System::Drawing::Size(100, 13);
              this->label3->TabIndex = 8;
              this->label3->Text = L"Apellido Paterno";
              // 
              // label4
              // 
              this->label4->AutoSize = true;
              this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label4->Location = System::Drawing::Point(12, 135);
              this->label4->Name = L"label4";
              this->label4->Size = System::Drawing::Size(102, 13);
              this->label4->TabIndex = 9;
              this->label4->Text = L"Apellido Materno";
              // 
              // label5
              // 
              this->label5->AutoSize = true;
              this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label5->Location = System::Drawing::Point(12, 165);
              this->label5->Name = L"label5";
              this->label5->Size = System::Drawing::Size(61, 13);
              this->label5->TabIndex = 10;
              this->label5->Text = L"Direcci�n";
              // 
              // direccionText
              // 
              this->direccionText->Location = System::Drawing::Point(117, 158);
              this->direccionText->Name = L"direccionText";
              this->direccionText->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
              this->direccionText->Size = System::Drawing::Size(197, 20);
              this->direccionText->TabIndex = 11;
              this->direccionText->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Usuario::direccionText_KeyPress);
              // 
              // telefonoText
              // 
              this->telefonoText->Location = System::Drawing::Point(117, 188);
              this->telefonoText->MaxLength = 10;
              this->telefonoText->Name = L"telefonoText";
              this->telefonoText->Size = System::Drawing::Size(197, 20);
              this->telefonoText->TabIndex = 12;
              this->telefonoText->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Usuario::telefonoText_KeyPress);
              // 
              // label6
              // 
              this->label6->AutoSize = true;
              this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label6->Location = System::Drawing::Point(12, 191);
              this->label6->Name = L"label6";
              this->label6->Size = System::Drawing::Size(57, 13);
              this->label6->TabIndex = 13;
              this->label6->Text = L"Telefono";
              // 
              // label7
              // 
              this->label7->AutoSize = true;
              this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->label7->Location = System::Drawing::Point(12, 221);
              this->label7->Name = L"label7";
              this->label7->Size = System::Drawing::Size(46, 13);
              this->label7->TabIndex = 14;
              this->label7->Text = L"Estado";
              // 
              // modificarBTN
              // 
              this->modificarBTN->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->modificarBTN->Location = System::Drawing::Point(139, 271);
              this->modificarBTN->Name = L"modificarBTN";
              this->modificarBTN->Size = System::Drawing::Size(75, 23);
              this->modificarBTN->TabIndex = 18;
              this->modificarBTN->Text = L"Actualizar";
              this->modificarBTN->UseVisualStyleBackColor = true;
              this->modificarBTN->Click += gcnew System::EventHandler(this, &Usuario::modificarBTN_Click);
              // 
              // estadoCombo
              // 
              this->estadoCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
              this->estadoCombo->FormattingEnabled = true;
              this->estadoCombo->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Inactivo", L"Activo" });
              this->estadoCombo->Location = System::Drawing::Point(117, 221);
              this->estadoCombo->Name = L"estadoCombo";
              this->estadoCombo->Size = System::Drawing::Size(116, 21);
              this->estadoCombo->TabIndex = 13;
              // 
              // borrarBTN
              // 
              this->borrarBTN->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->borrarBTN->Location = System::Drawing::Point(244, 271);
              this->borrarBTN->Name = L"borrarBTN";
              this->borrarBTN->Size = System::Drawing::Size(75, 23);
              this->borrarBTN->TabIndex = 25;
              this->borrarBTN->Text = L"Nuevo";
              this->borrarBTN->UseVisualStyleBackColor = true;
              this->borrarBTN->Click += gcnew System::EventHandler(this, &Usuario::borrarBTN_Click);
              // 
              // mostartBTN
              // 
              this->mostartBTN->Anchor = System::Windows::Forms::AnchorStyles::Bottom;
              this->mostartBTN->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->mostartBTN->Location = System::Drawing::Point(342, 378);
              this->mostartBTN->Name = L"mostartBTN";
              this->mostartBTN->Size = System::Drawing::Size(97, 23);
              this->mostartBTN->TabIndex = 26;
              this->mostartBTN->Text = L"Mostrar Todos";
              this->mostartBTN->UseVisualStyleBackColor = true;
              this->mostartBTN->Click += gcnew System::EventHandler(this, &Usuario::mostartBTN_Click);
              // 
              // Usuario
              // 
              this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
              this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
              this->ClientSize = System::Drawing::Size(1074, 413);
              this->Controls->Add(this->mostartBTN);
              this->Controls->Add(this->borrarBTN);
              this->Controls->Add(this->estadoCombo);
              this->Controls->Add(this->buscarTextBox);
              this->Controls->Add(this->buscarBTN);
              this->Controls->Add(this->tipoBusquedaCOMBOBOX);
              this->Controls->Add(this->label8);
              this->Controls->Add(this->modificarBTN);
              this->Controls->Add(this->dataGridUsuarios);
              this->Controls->Add(this->cancelBTN);
              this->Controls->Add(this->label7);
              this->Controls->Add(this->label6);
              this->Controls->Add(this->telefonoText);
              this->Controls->Add(this->direccionText);
              this->Controls->Add(this->label5);
              this->Controls->Add(this->label4);
              this->Controls->Add(this->label3);
              this->Controls->Add(this->apMatText);
              this->Controls->Add(this->apPatText);
              this->Controls->Add(this->noControlText);
              this->Controls->Add(this->label2);
              this->Controls->Add(this->nombreText);
              this->Controls->Add(this->agregarBTN);
              this->Controls->Add(this->label1);
              this->Name = L"Usuario";
              this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
              this->Text = L"Usuarios";
              this->Load += gcnew System::EventHandler(this, &Usuario::Usuario_Load);
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridUsuarios))->EndInit();
              this->ResumeLayout(false);
              this->PerformLayout();

          }
#pragma endregion
     private: System::Void Usuario_Load(System::Object^  sender, System::EventArgs^  e) {
         ActualizaTabla();
         tipoBusquedaCOMBOBOX->SelectedIndex = 1;
     }
     

     private: System::Void agregarBTN_Click(System::Object^  sender, System::EventArgs^  e) {             
         if (!ValidaVacios()) return;
             String ^noControl =noControlText->Text;
             String ^ nombre = nombreText->Text;
             String ^ apPaterno = apPatText->Text;
             String ^ apMaterno = apMatText->Text;
             String ^ direccion = direccionText->Text;
             String ^ telefono = telefonoText->Text;
             int estado = Convert::ToInt32( estadoCombo->SelectedIndex);
             Conexion::InsertarUsuario(noControl,nombre,apPaterno,apMaterno,direccion,telefono,estado);          
             LimpiarCampos();
             ActualizaTabla();

}
private: System::Void cancelBTN_Click(System::Object^  sender, System::EventArgs^  e) {
    this->Close();
}
private: System::Void noControlText_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {    
    if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter)) {
        if (noControlText->Text->Length!=0)
        nombreText->Focus();
        else
            MessageBox::Show("No debe Dejar vac�o el Compo Matricula");
    }
    if (Char::IsDigit(e->KeyChar)|| e->KeyChar==Convert::ToChar(System::Windows::Forms::Keys::Back)) {
        e->Handled = false;
    }
    else {
        e->Handled = true;
    }
}
private: System::Void telefonoText_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
    if (Char::IsDigit(e->KeyChar) || e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Back)) {
        e->Handled = false;
    }
    else {
        e->Handled = true;
    }
}

private: System::Void ActualizaTabla() {
         //Conexion c = Conexion::Conexion();
         //String^ constr = c.getStringConexion();
         //MySql::Data::MySqlClient::MySqlConnection^ con = gcnew MySql::Data::MySqlClient::MySqlConnection(constr);
         MySql::Data::MySqlClient::MySqlDataAdapter^ sda = gcnew MySql::Data::MySqlClient::MySqlDataAdapter("call consultaUsuarios()", conn);
         DataTable^ dt = gcnew DataTable();
         sda->Fill(dt);
         dataGridUsuarios ->DataSource = dt;
     }

private: System::Void BuscaUsuarios() {
    String^ busc = buscarTextBox->Text;
   // Conexion c = Conexion::Conexion();
    //String^ constr = c.getStringConexion();
    DataTable^ dt = gcnew DataTable();
    //MySql::Data::MySqlClient::MySqlConnection^ con = gcnew MySql::Data::MySqlClient::MySqlConnection(constr);
    //con->Open();
    MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand("", conn);
    cmd->CommandType = System::Data::CommandType::StoredProcedure;

    switch (tipoBusquedaCOMBOBOX->SelectedIndex)
    {
    case 0 :
        cmd->CommandText = "buscaUsuarioID";
        cmd->Parameters->AddWithValue("@iID",busc);

        break;
    case 1:
        cmd->CommandText = "buscaUsuarioNombre";
        cmd->Parameters->AddWithValue("@inombre", busc);
        break;
    case 2:
        cmd->CommandText = "buscaUsuarioApPaterno";
        cmd->Parameters->AddWithValue("@iApPaterno", busc);
        break;
    }
    MySql::Data::MySqlClient::MySqlDataReader^ myReader = cmd->ExecuteReader();
    //DataTable^ dt = gcnew DataTable();
    dt->Load(myReader);
    dataGridUsuarios->DataSource = dt;
    myReader->Close();
        //con->Close();
 }
private: System::Void dataGridUsuarios_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
    noControlText->Enabled = false;
    int indexRow = e->RowIndex >= 0 ? e->RowIndex : 0;
    DataGridViewRow^ rowSelected = dataGridUsuarios->Rows[indexRow];
    noControlText->Text = Convert::ToString(rowSelected->Cells[0]->Value);
    nombreText->Text = Convert::ToString(rowSelected->Cells[1]->Value);
    apPatText->Text = Convert::ToString(rowSelected->Cells[2]->Value);
    apMatText->Text = Convert::ToString(rowSelected->Cells[3]->Value);
    direccionText->Text = Convert::ToString(rowSelected->Cells[4]->Value);
    telefonoText->Text = Convert::ToString(rowSelected->Cells[5]->Value);
    estadoCombo->SelectedIndex= Convert::ToInt32(rowSelected->Cells[6]->Value);
}
private: System::Void buscarBTN_Click(System::Object^  sender, System::EventArgs^  e) {
    if (buscarTextBox->Text->Length == 0) {
        MessageBox::Show("Ingrese un dato para buscar");
        buscarTextBox->Focus();
        return;
    }
    BuscaUsuarios();
    buscarTextBox->Text = "";
}
private: System::Void nombreText_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
    if (Char::IsDigit(e->KeyChar)) {
        e->Handled = true;
    }
    else {
        e->Handled = false;
    }
    if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter)) {
        if (nombreText->Text->Length != 0)
            apPatText->Focus();
        else
            MessageBox::Show("No debe Dejar vac�o el Compo Nombre");
    }
}
private: System::Void apMatText_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
    if (Char::IsDigit(e->KeyChar)) {
        e->Handled = true;
    }
    else {
        e->Handled = false;
    }
    if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter)) {        
            direccionText->Focus();        
    }
}
private: System::Void direccionText_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
    if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter)) {
        if (direccionText->Text->Length != 0)
            telefonoText->Focus();
        else
            MessageBox::Show("No debe Dejar vac�o el Compo Direcci�n");
    }
}
private: System::Void apPatText_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
    if (Char::IsDigit(e->KeyChar)) {
        e->Handled = true;
    }
    else {
        e->Handled = false;
    }
    if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter)) {
        if (apPatText->Text->Length != 0)
            apMatText->Focus();
        else
            MessageBox::Show("No debe Dejar vac�o el Compo Apellido Paterno");
    }
}


private: System::Void borrarBTN_Click(System::Object^  sender, System::EventArgs^  e) {
    LimpiarCampos();
    noControlText->Enabled = true;
}
private: System::Void mostartBTN_Click(System::Object^  sender, System::EventArgs^  e) {
    ActualizaTabla();
}

     private:System::Boolean ValidaVacios() {
         if(noControlText->Text->Length==0){
             noControlText->Focus();
             MessageBox::Show("No debe dejar Vacio el campo Matricula");
             return false;
         }
         if (nombreText->Text->Length == 0) {
             nombreText->Focus();
             MessageBox::Show("No debe dejar Vacio el campo Nombre");
             return false;
         }
         if (apPatText->Text->Length == 0) {
             apPatText->Focus();
             MessageBox::Show("No debe dejar Vacio el campo apellido paterno");
             return false;
         }
         if (direccionText->Text->Length == 0) {
             direccionText->Focus();
             MessageBox::Show("No debe dejar Vacio el campo direcci�n");
             return false;
         }
         return true;
        }
    private:System::Void LimpiarCampos() {
        noControlText->Text = "";
        nombreText->Text = "";
        apPatText->Text = "";
        apMatText->Text = "";
        direccionText->Text = "";
        telefonoText->Text = "";
        estadoCombo->Text = "";
    }
private: System::Void modificarBTN_Click(System::Object^  sender, System::EventArgs^  e) {
    if (!ValidaVacios()) return;
    String ^noControl = noControlText->Text;
    String ^ nombre = nombreText->Text;
    String ^ apPaterno = apPatText->Text;
    String ^ apMaterno = apMatText->Text;
    String ^ direccion = direccionText->Text;
    String ^ telefono = telefonoText->Text;
    int estado = Convert::ToInt32(estadoCombo->SelectedIndex);
    Conexion::ActualizaUsuario(noControl, nombre, apPaterno, apMaterno, direccion, telefono, estado);
    LimpiarCampos();
    ActualizaTabla();
}
};
}
