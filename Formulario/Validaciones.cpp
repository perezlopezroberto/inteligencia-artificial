#include "Validaciones.h"
using namespace System::Windows::Forms;
using namespace System;

/*
Validaciones::Validaciones()
{
}*/


bool Validaciones::validarPlaca(System::String^placa)
{
	System::String^aux;
	aux = placa->Substring(0,1);
	bool band = false;
	if (aux->Equals("0"))
	{
		//System::Windows::Forms::MessageBox::Show("Placa no debe iniciar con 0");
		band = true;
	}
	return band;
}


//Metodo para que solo acepte letras
       void Validaciones::revisarLetras(System::Windows::Forms::KeyPressEventArgs^  e)
		{
			if (Char::IsDigit(e->KeyChar))
			{
				e->Handled = true;
			}
			else if (Char::IsControl(e->KeyChar))
			{
				e->Handled = false;
			}
			else if (Char::IsSeparator(e->KeyChar))
			{
				e->Handled = false;
			}
			else
			{
				e->Handled = false;
			}

		}
				//metodo que solo aceote numeros
	   void Validaciones::revisarNumeros(System::Windows::Forms::KeyPressEventArgs^  e)
		{
			if (Char::IsDigit(e->KeyChar))
			{
				e->Handled = false;
			}
			else if (Char::IsControl(e->KeyChar))
			{
				e->Handled = false;
			}
			else if (Char::IsSeparator(e->KeyChar))
			{
				e->Handled = false;
			}
			else
			{
				e->Handled = true;
			}
		}