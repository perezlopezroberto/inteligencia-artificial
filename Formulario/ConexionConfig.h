#pragma once
#include "Conexion.h"
namespace Formulario {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Resumen de ConexionConfig
	/// </summary>
	public ref class ConexionConfig : public System::Windows::Forms::Form
	{
	public:
		ConexionConfig(void)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~ConexionConfig()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  hostTxBx;
	private: System::Windows::Forms::TextBox^  userTxBx;
	private: System::Windows::Forms::TextBox^  passwdTxBx;
	private: System::Windows::Forms::TextBox^  databaseTxBx;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  testConBtn;
	private: System::Windows::Forms::Button^  guardarBtn;
	protected:

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ConexionConfig::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->hostTxBx = (gcnew System::Windows::Forms::TextBox());
			this->userTxBx = (gcnew System::Windows::Forms::TextBox());
			this->passwdTxBx = (gcnew System::Windows::Forms::TextBox());
			this->databaseTxBx = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->testConBtn = (gcnew System::Windows::Forms::Button());
			this->guardarBtn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(144, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Configuraci�n de la conexi�n";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(60, 49);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(27, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"host";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(60, 95);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(27, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"user";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(61, 143);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(52, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"password";
			// 
			// hostTxBx
			// 
			this->hostTxBx->Location = System::Drawing::Point(150, 42);
			this->hostTxBx->Name = L"hostTxBx";
			this->hostTxBx->Size = System::Drawing::Size(100, 20);
			this->hostTxBx->TabIndex = 4;
			this->hostTxBx->Text = L"localhost";
			this->hostTxBx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &ConexionConfig::teclaPresionadaGeneral);
			// 
			// userTxBx
			// 
			this->userTxBx->Location = System::Drawing::Point(150, 88);
			this->userTxBx->Name = L"userTxBx";
			this->userTxBx->Size = System::Drawing::Size(100, 20);
			this->userTxBx->TabIndex = 5;
			this->userTxBx->Text = L"root";
			this->userTxBx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &ConexionConfig::teclaPresionadaGeneral);
			// 
			// passwdTxBx
			// 
			this->passwdTxBx->Location = System::Drawing::Point(150, 136);
			this->passwdTxBx->Name = L"passwdTxBx";
			this->passwdTxBx->Size = System::Drawing::Size(100, 20);
			this->passwdTxBx->TabIndex = 6;
			this->passwdTxBx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &ConexionConfig::teclaPresionadaGeneral);
			// 
			// databaseTxBx
			// 
			this->databaseTxBx->Location = System::Drawing::Point(150, 184);
			this->databaseTxBx->Name = L"databaseTxBx";
			this->databaseTxBx->Size = System::Drawing::Size(100, 20);
			this->databaseTxBx->TabIndex = 7;
			this->databaseTxBx->Text = L"estacionamiento";
			this->databaseTxBx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &ConexionConfig::teclaPresionadaGeneral);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(61, 191);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(51, 13);
			this->label5->TabIndex = 8;
			this->label5->Text = L"database";
			// 
			// testConBtn
			// 
			this->testConBtn->Location = System::Drawing::Point(63, 236);
			this->testConBtn->Name = L"testConBtn";
			this->testConBtn->Size = System::Drawing::Size(187, 23);
			this->testConBtn->TabIndex = 9;
			this->testConBtn->Text = L"&Probar Conexi�n";
			this->testConBtn->UseVisualStyleBackColor = true;
			this->testConBtn->Click += gcnew System::EventHandler(this, &ConexionConfig::testConBtn_Click);
			// 
			// guardarBtn
			// 
			this->guardarBtn->Location = System::Drawing::Point(63, 281);
			this->guardarBtn->Name = L"guardarBtn";
			this->guardarBtn->Size = System::Drawing::Size(187, 23);
			this->guardarBtn->TabIndex = 10;
			this->guardarBtn->Text = L"&Guardar y Salir";
			this->guardarBtn->UseVisualStyleBackColor = true;
			this->guardarBtn->Click += gcnew System::EventHandler(this, &ConexionConfig::guardarBtn_Click);
			// 
			// ConexionConfig
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(317, 333);
			this->Controls->Add(this->guardarBtn);
			this->Controls->Add(this->testConBtn);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->databaseTxBx);
			this->Controls->Add(this->passwdTxBx);
			this->Controls->Add(this->userTxBx);
			this->Controls->Add(this->hostTxBx);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"ConexionConfig";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"ConexionConfig";
			this->Load += gcnew System::EventHandler(this, &ConexionConfig::ConexionConfig_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void testConBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		String^ server = hostTxBx->Text;
		String^ user = userTxBx->Text;
		String^ passwd = passwdTxBx->Text;
		String^ database = databaseTxBx->Text;
		Conexion::pruebaConexion(server, user,database,passwd);
	}
private: System::Void guardarBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	String^ server = hostTxBx->Text;
	String^ user = userTxBx->Text;
	String^ passwd = passwdTxBx->Text;
	String^ database = databaseTxBx->Text;
	String^ constring = L"server=" + server + ";user=" + user + ";port=3306;database=" + database + ";password=" + passwd + ";";
	//cadena = constring;
	try
	{
		//Pass the file path and file name to the StreamWriter Constructor.
		System::IO::StreamWriter^ sw = gcnew System::IO::StreamWriter("./conn.stored");
		//Write a line of text.
		sw->WriteLine(constring);
		//Close the file.
		sw->Close();
	}
	catch (Exception^ e)
	{
		Console::WriteLine("Exception: {0}", e->Message);
	}
	this->Close();
}
private: System::Void teclaPresionadaGeneral(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		SendKeys::Send("{TAB}");
		e->Handled = true;
	}
}
private: System::Void ConexionConfig_Load(System::Object^  sender, System::EventArgs^  e) {
}
};
}
