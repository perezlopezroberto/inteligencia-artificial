#pragma once
#ifndef Conexion_H
#define Conexion_H
#include <string>
#include <iostream>
using namespace std;
using namespace MySql::Data::MySqlClient;
class Conexion
{
private:
public:
    //Conexion();        
	static MySqlConnection^ getConnectionObj();
	static void pruebaConexion(System::String^ server, System::String^ user, System::String^ database, System::String^ passwd);
	static System::String^ getStringConexion();
	static bool checkLogin(System::String^ usuario, System::String^ clave, MySqlConnection^ conn);
	static System::String^ checkLoginP(System::String^ usuario, System::String^ clave, MySqlConnection^ conn);
	static void agregarLoginUser(System::String^ nombre, System::String^ passwd, System::String^ rol, MySqlConnection^ conn);
	static void actualizaLoginUser(System::String^ nombre, System::String^ passwd, System::String^ rol,MySqlConnection^ conn);
	static void eliminarLoginUser(System::String^ nombre, MySqlConnection^ conn);
	static void insertarVehiculo(System::String^placa, System::String^ marca, System::String^ color, System::String^ tipo, System::String^matricula, MySqlConnection^ conn);
	static void EliminarVehiculo(int id, MySqlConnection^ conn);
	static void SalidaPeaton(int id);
	static void InsertarPeatonE(System::String^nombre, System::String ^apaterno, System::String ^amaterno, System::String^horaEntrada, System::String^motivoVisita);
	static void ActualizarVehiculo(int id, System::String^placa, System::String^ marca, System::String^ color, System::String^ tipo, System::String^matricula, MySqlConnection^ conn);
     //usuario
	static void InsertarUsuario(System::String^ matricula, System::String^nombre, System::String ^apapaterno, System::String ^apmaterno, System::String^direccion, System::String^telefono,int estado);
	static void ActualizaUsuario(System::String^ matricula, System::String^nombre, System::String ^apapaterno, System::String ^apmaterno, System::String^direccion, System::String^telefono, int estado);
	static System::DateTime^ getFechaSistema(MySqlConnection^ conn);
	static void insertarAccesoSistema(System::String^ usuario, MySqlConnection^ conn);
	static int getUltimoAccesoSistema(MySqlConnection^ conn);
	static void actualizaHoraSalidaSistema(int id,MySqlConnection^ conn);
	static int iniciarSistema(System::String^ usuario, MySqlConnection^ conn);
};

#endif // !Conexion_H
