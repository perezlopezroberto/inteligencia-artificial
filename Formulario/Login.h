#pragma once
#include "Conexion.h"
#include "ControlAccesoVehicular.h"
#include "ConexionConfig.h"

namespace Formulario {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Resumen de Login
	/// </summary>
	public ref class Login : public System::Windows::Forms::Form
	{
	public:
		Login(void)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}
	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Login()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  usuarioTxbx;
	private: System::Windows::Forms::TextBox^  passwordTxBx;
	private: System::Windows::Forms::Button^  connConfig;
	private: System::Windows::Forms::Button^  logInBtn;
	private: MySql::Data::MySqlClient::MySqlConnection^ conn;
	protected:

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Login::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->usuarioTxbx = (gcnew System::Windows::Forms::TextBox());
			this->passwordTxBx = (gcnew System::Windows::Forms::TextBox());
			this->logInBtn = (gcnew System::Windows::Forms::Button());
			this->connConfig = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(111, 32);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(100, 100);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(60, 149);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(43, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Usuario";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(42, 179);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(61, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Contrase�a";
			// 
			// usuarioTxbx
			// 
			this->usuarioTxbx->Location = System::Drawing::Point(111, 142);
			this->usuarioTxbx->MaxLength = 15;
			this->usuarioTxbx->Name = L"usuarioTxbx";
			this->usuarioTxbx->Size = System::Drawing::Size(100, 20);
			this->usuarioTxbx->TabIndex = 3;
			this->usuarioTxbx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Login::usuarioTxbx_KeyPress);
			// 
			// passwordTxBx
			// 
			this->passwordTxBx->Location = System::Drawing::Point(111, 172);
			this->passwordTxBx->MaxLength = 20;
			this->passwordTxBx->Name = L"passwordTxBx";
			this->passwordTxBx->PasswordChar = '*';
			this->passwordTxBx->Size = System::Drawing::Size(100, 20);
			this->passwordTxBx->TabIndex = 4;
			this->passwordTxBx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Login::passwordTxBx_KeyPress);
			// 
			// logInBtn
			// 
			this->logInBtn->Location = System::Drawing::Point(111, 212);
			this->logInBtn->Name = L"logInBtn";
			this->logInBtn->Size = System::Drawing::Size(100, 23);
			this->logInBtn->TabIndex = 5;
			this->logInBtn->Text = L"&Iniciar Sesi�n";
			this->logInBtn->UseVisualStyleBackColor = true;
			this->logInBtn->Click += gcnew System::EventHandler(this, &Login::logInBtn_Click);
			// 
			// connConfig
			// 
			this->connConfig->Location = System::Drawing::Point(280, 12);
			this->connConfig->Name = L"connConfig";
			this->connConfig->Size = System::Drawing::Size(22, 21);
			this->connConfig->TabIndex = 6;
			this->connConfig->Text = L"&\?";
			this->connConfig->UseVisualStyleBackColor = true;
			this->connConfig->Click += gcnew System::EventHandler(this, &Login::connConfig_Click);
			// 
			// Login
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(314, 282);
			this->Controls->Add(this->connConfig);
			this->Controls->Add(this->logInBtn);
			this->Controls->Add(this->passwordTxBx);
			this->Controls->Add(this->usuarioTxbx);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pictureBox1);
			this->Cursor = System::Windows::Forms::Cursors::Hand;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"Login";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Login";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Login::Login_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Login::Login_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void logInBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		revisarLogin();
	}
private: System::Void connConfig_Click(System::Object^  sender, System::EventArgs^  e) {
	ConexionConfig^ ventanaConfig = gcnew ConexionConfig();
	ventanaConfig->ShowDialog();
	iniciarConexion();
}
		 
private: System::Void passwordTxBx_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		revisarLogin();
	}
}
		 private: System::Void revisarLogin() {
			 if (!revisarTextoVacio()) return;
			 String^ rol = Conexion::checkLoginP(usuarioTxbx->Text, passwordTxBx->Text, this->conn);
			 if (rol != "false")
			 {
				 ControlAccesoVehicular^ ventana = gcnew ControlAccesoVehicular(this->conn,usuarioTxbx->Text, rol);
				 usuarioTxbx->Text = "";
				 passwordTxBx->Text = "";
				 this->Hide();
				 ventana->ShowDialog();
				 this->Show();
				 usuarioTxbx->Focus();
			 }
			 else MessageBox::Show("Datos incorrectos");
		 }
		private: bool revisarTextoVacio() {
			if (usuarioTxbx->Text == "")
			{
				MessageBox::Show("Rellenar campo de usuario por favor");
				usuarioTxbx->Focus();
				return false;
			}
			else if (passwordTxBx->Text == "")
			{
				MessageBox::Show("Rellenar campo contrase�a por favor");
				passwordTxBx->Focus();
				return false;
			}else  return true;
		}
private: System::Void usuarioTxbx_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		passwordTxBx->Focus();
	}
}
private: System::Void Login_Load(System::Object^  sender, System::EventArgs^  e) {
	iniciarConexion();
}
private: System::Void Login_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	conn->Close();
}
		 private: System::Void iniciarConexion() {
			 this->conn = Conexion::getConnectionObj();
			 try
			 {
				 conn->Open();
				 //MessageBox::Show("Conexi�n establecida correctamente");
			 }
			 catch (MySqlException^ mye) {
				 MessageBox::Show("Resuelva los problemas de conexion en la ventana de configuraci�n\n"+ mye->Message);
			 }
			 catch (Exception^ ex) {
				 MessageBox::Show(ex->Message);
			 }
		 }
};
}
