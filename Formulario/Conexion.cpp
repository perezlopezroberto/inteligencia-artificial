#pragma once
#include "Conexion.h"
using namespace System;
using namespace System::Windows::Forms;
using namespace MySql::Data::MySqlClient;

/*Conexion::Conexion()
{	
}*/
//Metodo para insertar vehiculo a la base de datos
void Conexion::insertarVehiculo(System::String^placa, System::String^ marca, System::String^ color, System::String^ tipo, System::String^matricula, MySqlConnection^ conn)
{
	try {
		String^ rtn = "insertarVehiculo";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@iplaca", placa);
		cmd->Parameters->AddWithValue("@imarca", marca);
		cmd->Parameters->AddWithValue("@icolor", color);
		cmd->Parameters->AddWithValue("@itipo", tipo);
		cmd->Parameters->AddWithValue("@imatricula", matricula);

		int result = cmd->ExecuteNonQuery();
		if (result == 1)MessageBox::Show("Insertado Correctamente");
	}
	catch (Exception^ ex)
	{
		MessageBox::Show("Error en funcion insertarVehiculo() -->"+ex->Message);
	}
}

//Metodo para Eliminar Vehiculo de la base de datos
void Conexion::EliminarVehiculo(int id, MySqlConnection^ conn)
{
	try {
		String^ rtn = "borrarVehiculo";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		cmd->Parameters->AddWithValue("@id", id);

		int result = cmd->ExecuteNonQuery();
		if (result == 1)MessageBox::Show("Eliminado Correctamente");
		else if (result == 0)MessageBox::Show("No se realiz� ning�n cambio");
	}
	catch (Exception^ ex) { MessageBox::Show("Error en la funci�n EliminarVehiculo(): " + ex->Message); }
}

//metodo que actualiza datos del vehiculo de la BD
void Conexion::ActualizarVehiculo(int id, System::String^placa, System::String^ marca, System::String^ color, System::String^ tipo, System::String^matricula, MySqlConnection^ conn)
{
	try {
		String^ rtn = "actualizarVehiculo";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		cmd->Parameters->AddWithValue("@id", id);
		cmd->Parameters->AddWithValue("@placa1", placa);
		cmd->Parameters->AddWithValue("@marca1", marca);
		cmd->Parameters->AddWithValue("@color1", color);
		cmd->Parameters->AddWithValue("@tipo1", tipo);
		cmd->Parameters->AddWithValue("@matricula1", matricula);

		int result = cmd->ExecuteNonQuery();
		if (result == 1)MessageBox::Show("Actualizado Correctamente");
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}
void Conexion::SalidaPeaton(int id)
{
	try {
		String^conecction = getStringConexion();
		MySqlConnection^ con = gcnew MySqlConnection(conecction);//establece los parametros para la conexion
		MySqlCommand^ cmd = gcnew MySqlCommand("UPDATE entradapeaton SET horaSalida=NOW() WHERE folio=" + id, con);
		con->Open();
		MySqlDataReader^ dr = cmd->ExecuteReader();
		MessageBox::Show("Vehiculo Info Updated");
		con->Close();
	}
	catch (Exception ^ ex) {
		MessageBox::Show(ex->Message);
	}
}
void Conexion::InsertarUsuario(System::String^ matricula, System::String^nombre, System::String ^apapaterno, System::String ^apmaterno, System::String^direccion, System::String^telefono,int estado){
   
    String^ constr = getStringConexion();
    MySqlConnection^ conn = gcnew MySqlConnection(constr);
    try {
        conn->Open();
        String^ rtn = "insertaUsuario";
        MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
        cmd->CommandType = System::Data::CommandType::StoredProcedure;
        cmd->Parameters->AddWithValue("@matri",matricula);
        cmd->Parameters->AddWithValue("@nomb",nombre);
        cmd->Parameters->AddWithValue("@apat",apapaterno);
        cmd->Parameters->AddWithValue("@amater",apmaterno);
        cmd->Parameters->AddWithValue("@direc",direccion);
        cmd->Parameters->AddWithValue("@tel",telefono);
        cmd->Parameters->AddWithValue("@iestado",estado);

        int result = cmd->ExecuteNonQuery();
        if (result == 1)MessageBox::Show("Insertado Correctamente");
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(ex->Message);
    }
    conn->Close();

}
void Conexion::ActualizaUsuario(System::String^ matricula, System::String^nombre, System::String ^apapaterno, System::String ^apmaterno, System::String^direccion, System::String^telefono, int estado) {

    String^ constr = getStringConexion();
    MySqlConnection^ conn = gcnew MySqlConnection(constr);
    try {
        conn->Open();
        String^ rtn = "actualizaUsuario";
        MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
        cmd->CommandType = System::Data::CommandType::StoredProcedure;
        cmd->Parameters->AddWithValue("@matric", matricula);
        cmd->Parameters->AddWithValue("@nomb", nombre);
        cmd->Parameters->AddWithValue("@apat", apapaterno);
        cmd->Parameters->AddWithValue("@amater", apmaterno);
        cmd->Parameters->AddWithValue("@direc", direccion);
        cmd->Parameters->AddWithValue("@tel", telefono);
        cmd->Parameters->AddWithValue("@iestado",estado);

        int result = cmd->ExecuteNonQuery();
        if (result == 1)MessageBox::Show("Actualizado Correctamente");
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(ex->Message);
    }
    conn->Close();

}
void Conexion::InsertarPeatonE(System::String^nombre, System::String ^apaterno, System::String ^amaterno, System::String^horaEntrada, System::String^motivoVisita) {
	try {
		String^conecction = getStringConexion();
		MySqlConnection^ con = gcnew MySqlConnection(conecction);//establece los parametros para la conexion
		MySqlCommand^ cmd = gcnew MySqlCommand("INSERT INTO `estacionamiento`.`entradapeaton` (`nombre`, `apaterno`, `amaterno`, `horaEntrada`, `motivoVisita`) VALUES ('" + nombre + "','" + apaterno + "','" + amaterno + "','" + horaEntrada + "','" + motivoVisita + "')", con);//parametros que recibira la consulta
		con->Open();//abre la conexion
		MySqlDataReader^ dr = cmd->ExecuteReader();//ejecuta la solicitud del sql
		MessageBox::Show("Insertado");//Muestra mensaje de que se inserto el datos
		con->Close();//cierra la conexion
	}
	catch (Exception ^ ex) {
		MessageBox::Show(ex->Message);
	}
}
void Conexion::pruebaConexion(String^ server, String^ user, String^ database, String^ passwd) {
	String^ constring = "server=" + server + ";user=" + user + ";port=3306;database=" + database + ";password=" + passwd + ";";
	MySqlConnection^ con = gcnew MySqlConnection(constring);
	try
	{
		con->Open();
		MessageBox::Show("Conexi�n establecida correctamente");
	}
	catch (MySqlException^ mye) {
			MessageBox::Show(mye->Message);
	}
	catch (Exception^ ex) {
		MessageBox::Show(ex->Message);
	}
	con->Close();
}
System::String^ Conexion::getStringConexion() {
	String^ cadena;
	try
	{
		//Pass the file path and file name to the StreamReader constructor.
		System::IO::StreamReader^ sr = gcnew System::IO::StreamReader("./conn.stored");
		//Read the first line of text.
		cadena = sr->ReadLine();
		//Close the file.
		sr->Close();
	}
	catch (Exception^ e)
	{
		Console::WriteLine("Exception: {0}", e->Message);
		cadena = L"server=localhost;user=root;port=3306;database=estacionamiento;password=raul;";
	}
	return cadena;
}
bool Conexion::checkLogin(String^ usuario, String^ clave, MySqlConnection^ conn) {
	bool res = false;
	try
	{
		MySqlCommand^ cmdDataBase = gcnew MySqlCommand("SELECT * FROM login WHERE nombre=@Name AND password=@Password;", conn);
		MySqlDataReader^ myReader;
		//and then add 2 new lines of code like so
		cmdDataBase->Parameters->AddWithValue("@Name", usuario);
		cmdDataBase->Parameters->AddWithValue("@Password", clave);
		//You can add this code in between
		myReader = cmdDataBase->ExecuteReader();
		res = myReader->HasRows;
		myReader->Close();
	}
	catch (Exception^ ex) {
		MessageBox::Show(ex->Message);
	}
	return res;
}
String^ Conexion::checkLoginP(String^ usuario, String^ clave, MySqlConnection^ conn) {
	String^ res = "false";
	try
	{
		String^ rtn = "checkLogin";
		MySqlCommand^ cmd = gcnew MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		cmd->Parameters->AddWithValue("@inombre", usuario);
		cmd->Parameters->AddWithValue("@ipasswd", clave);
		//You can add this code in between
		MySqlDataReader^ myReader = cmd->ExecuteReader();
		
		if (myReader->HasRows) {
			myReader->Read();
			res = myReader[1]->ToString();
		}
		myReader->Close();
	}
	catch (Exception^ ex) {
		MessageBox::Show(ex->Message);
	}
	return res;
}
void Conexion::agregarLoginUser(String^ nombre, String^ passwd, String^ rol, MySqlConnection^ conn) {
	try {
		String^ rtn = "guardaLoginUser";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		cmd->Parameters->AddWithValue("@inombre", nombre);
		cmd->Parameters->AddWithValue("@ipassword", passwd);
		cmd->Parameters->AddWithValue("@irol", rol);

		int result = cmd->ExecuteNonQuery();
		if (result == 1)MessageBox::Show("Insertado Correctamente");
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}
void Conexion::actualizaLoginUser(String^ nombre, String^ passwd, String^ rol, MySqlConnection^ conn) {
	try {
		String^ rtn = "actualizaLoginUser";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		cmd->Parameters->AddWithValue("@inombre", nombre);
		cmd->Parameters->AddWithValue("@ipassword", passwd);
		cmd->Parameters->AddWithValue("@irol", rol);

		int result = cmd->ExecuteNonQuery();
		if (result == 1)MessageBox::Show("Actualizado Correctamente");
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}
void Conexion::eliminarLoginUser(String^ nombre, MySqlConnection^ conn) {
	try {
		String^ rtn = "borraLoginUser";
		MySqlCommand^ cmd = gcnew MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		cmd->Parameters->AddWithValue("@inombre", nombre);

		int result = cmd->ExecuteNonQuery();
		if (result == 1)MessageBox::Show("Eliminado Correctamente");
		else if (result == 0)MessageBox::Show("No se realiz� ning�n cambio");
	}
	catch (Exception^ ex) { MessageBox::Show(ex->Message); }
}

MySqlConnection^ Conexion::getConnectionObj() {
	MySqlConnection^ con = gcnew MySqlConnection(getStringConexion());
	return con;
}
DateTime^ Conexion::getFechaSistema(MySqlConnection^ conn) {
	try {
		String^ rtn = "getHoraYFecha";
		MySqlCommand^ cmd = gcnew MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		Object^ result = cmd->ExecuteScalar();
		if (result != nullptr)
		{
			DateTime^ r = Convert::ToDateTime(result);
			//Console::WriteLine("Obtuve del sistema: " + r);
			return r;
		}
	}
	catch (Exception^ ex) {
		MessageBox::Show(ex->Message);
	}
	return gcnew DateTime();
}
void Conexion::insertarAccesoSistema(System::String^ usuario, MySqlConnection^ conn) {
	try{
		String^ rtn = "insertar_accesosistema";
		MySqlCommand^ cmd = gcnew MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@inombre", usuario);
		int result = cmd->ExecuteNonQuery();
	}
	catch (Exception^ ex) { MessageBox::Show(ex->Message); }
}
int Conexion::getUltimoAccesoSistema(MySqlConnection^ conn) {
	try {
		String^ rtn = "obten_id_ultimo_acceso";
		MySqlCommand^ cmd = gcnew MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		Object^ result = cmd->ExecuteScalar();
		if (result != nullptr)
		{
			int r = Convert::ToInt16(result);
			return r;
		}
	}
	catch (Exception^ ex) { MessageBox::Show(ex->Message); }
	return 0;
}
void Conexion::actualizaHoraSalidaSistema(int id, MySqlConnection^ conn) {
	try {
		String^ rtn = "actualizaSalidaSistema";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@iid", id);
		int result = cmd->ExecuteNonQuery();
	}
	catch (Exception^ ex){MessageBox::Show(ex->Message);}
}
int Conexion::iniciarSistema(System::String^ usuario, MySqlConnection^ conn) {
	MySqlTransaction^ myTrans;

	// Start a local transaction
	myTrans = conn->BeginTransaction();
	// Must assign both transaction object and connection
	// to Command object for a pending local transaction

	
	try {
		String^ rtn = "insertar_accesosistema";
		MySqlCommand^ cmd = gcnew MySqlCommand(rtn, conn);
		cmd->Transaction = myTrans;
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@inombre", usuario);
		cmd->ExecuteNonQuery();
		cmd->CommandText = "obten_id_ultimo_acceso";
		cmd->Parameters->Clear();
		Object^ result = cmd->ExecuteScalar();
		myTrans->Commit();
		if (result != nullptr)
		{
			int r = Convert::ToInt16(result);
			return r;
		}
	}
	catch (Exception^ e) { 
		try
		{
			myTrans->Rollback();
		}
		catch (MySql::Data::MySqlClient::MySqlException^ ex)
		{
			if (myTrans->Connection)
			{
				Console::WriteLine("An exception of type " + ex->GetType() +
					" was encountered while attempting to roll back the transaction.");
			}
		}

		Console::WriteLine("An exception of type " + e->GetType() +
			" was encountered while inserting the data.");
		Console::WriteLine("Neither record was written to database.");
	}
}
