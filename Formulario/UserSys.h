#pragma once
#include "Conexion.h"
namespace Formulario {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Resumen de UserSys
	/// </summary>
	public ref class UserSys : public System::Windows::Forms::Form
	{
	public:
		UserSys(MySql::Data::MySqlClient::MySqlConnection^ conn)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
			this->conn = conn;
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~UserSys()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  nuevoBtn;
	private: System::Windows::Forms::Button^  agregarBtn;
	private: System::Windows::Forms::Button^  modificarBtn;
	private: System::Windows::Forms::Button^  eliminarBtn;
	private: System::Windows::Forms::TextBox^  nombreTxBx;
	private: System::Windows::Forms::TextBox^  passwordTxBx;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  actualizarBtn;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::TextBox^  busquedaTxBx;
	private: System::Windows::Forms::GroupBox^  busquedaCuadro;
	private: System::Windows::Forms::ComboBox^  rolCB;
	private: System::Windows::Forms::ComboBox^  campoBusquedaCB;
	private: MySql::Data::MySqlClient::MySqlConnection^ conn;

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(UserSys::typeid));
			this->nuevoBtn = (gcnew System::Windows::Forms::Button());
			this->agregarBtn = (gcnew System::Windows::Forms::Button());
			this->modificarBtn = (gcnew System::Windows::Forms::Button());
			this->eliminarBtn = (gcnew System::Windows::Forms::Button());
			this->nombreTxBx = (gcnew System::Windows::Forms::TextBox());
			this->passwordTxBx = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->actualizarBtn = (gcnew System::Windows::Forms::Button());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->busquedaTxBx = (gcnew System::Windows::Forms::TextBox());
			this->busquedaCuadro = (gcnew System::Windows::Forms::GroupBox());
			this->campoBusquedaCB = (gcnew System::Windows::Forms::ComboBox());
			this->rolCB = (gcnew System::Windows::Forms::ComboBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->busquedaCuadro->SuspendLayout();
			this->SuspendLayout();
			// 
			// nuevoBtn
			// 
			this->nuevoBtn->Location = System::Drawing::Point(29, 188);
			this->nuevoBtn->Name = L"nuevoBtn";
			this->nuevoBtn->Size = System::Drawing::Size(75, 23);
			this->nuevoBtn->TabIndex = 10;
			this->nuevoBtn->Text = L"Nuevo";
			this->nuevoBtn->UseVisualStyleBackColor = true;
			this->nuevoBtn->Click += gcnew System::EventHandler(this, &UserSys::nuevoBtn_Click);
			// 
			// agregarBtn
			// 
			this->agregarBtn->Location = System::Drawing::Point(29, 159);
			this->agregarBtn->Name = L"agregarBtn";
			this->agregarBtn->Size = System::Drawing::Size(75, 23);
			this->agregarBtn->TabIndex = 7;
			this->agregarBtn->Text = L"Agregar";
			this->agregarBtn->UseVisualStyleBackColor = true;
			this->agregarBtn->Click += gcnew System::EventHandler(this, &UserSys::agregarBtn_Click);
			// 
			// modificarBtn
			// 
			this->modificarBtn->Location = System::Drawing::Point(110, 159);
			this->modificarBtn->Name = L"modificarBtn";
			this->modificarBtn->Size = System::Drawing::Size(75, 23);
			this->modificarBtn->TabIndex = 8;
			this->modificarBtn->Text = L"Modificar";
			this->modificarBtn->UseVisualStyleBackColor = true;
			this->modificarBtn->Click += gcnew System::EventHandler(this, &UserSys::modificarBtn_Click);
			// 
			// eliminarBtn
			// 
			this->eliminarBtn->Location = System::Drawing::Point(191, 159);
			this->eliminarBtn->Name = L"eliminarBtn";
			this->eliminarBtn->Size = System::Drawing::Size(75, 23);
			this->eliminarBtn->TabIndex = 9;
			this->eliminarBtn->Text = L"Eliminar";
			this->eliminarBtn->UseVisualStyleBackColor = true;
			this->eliminarBtn->Click += gcnew System::EventHandler(this, &UserSys::eliminarBtn_Click);
			// 
			// nombreTxBx
			// 
			this->nombreTxBx->Location = System::Drawing::Point(79, 29);
			this->nombreTxBx->MaxLength = 15;
			this->nombreTxBx->Name = L"nombreTxBx";
			this->nombreTxBx->Size = System::Drawing::Size(100, 20);
			this->nombreTxBx->TabIndex = 4;
			// 
			// passwordTxBx
			// 
			this->passwordTxBx->Location = System::Drawing::Point(79, 68);
			this->passwordTxBx->MaxLength = 20;
			this->passwordTxBx->Name = L"passwordTxBx";
			this->passwordTxBx->Size = System::Drawing::Size(100, 20);
			this->passwordTxBx->TabIndex = 5;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(26, 32);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(47, 13);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Nombre:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(9, 71);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(64, 13);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Contrase�a:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(47, 113);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(26, 13);
			this->label3->TabIndex = 9;
			this->label3->Text = L"Rol:";
			// 
			// actualizarBtn
			// 
			this->actualizarBtn->Location = System::Drawing::Point(9, 45);
			this->actualizarBtn->Name = L"actualizarBtn";
			this->actualizarBtn->Size = System::Drawing::Size(152, 24);
			this->actualizarBtn->TabIndex = 14;
			this->actualizarBtn->Text = L"Buscar";
			this->actualizarBtn->UseVisualStyleBackColor = true;
			this->actualizarBtn->Click += gcnew System::EventHandler(this, &UserSys::actualizarBtn_Click);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(300, 12);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->Size = System::Drawing::Size(360, 184);
			this->dataGridView1->TabIndex = 11;
			this->dataGridView1->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &UserSys::dataGridView1_CellClick);
			// 
			// busquedaTxBx
			// 
			this->busquedaTxBx->Location = System::Drawing::Point(6, 19);
			this->busquedaTxBx->Name = L"busquedaTxBx";
			this->busquedaTxBx->Size = System::Drawing::Size(176, 20);
			this->busquedaTxBx->TabIndex = 12;
			this->busquedaTxBx->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &UserSys::busquedaTxBx_KeyPress);
			// 
			// busquedaCuadro
			// 
			this->busquedaCuadro->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->busquedaCuadro->Controls->Add(this->campoBusquedaCB);
			this->busquedaCuadro->Controls->Add(this->busquedaTxBx);
			this->busquedaCuadro->Controls->Add(this->actualizarBtn);
			this->busquedaCuadro->Location = System::Drawing::Point(294, 256);
			this->busquedaCuadro->Name = L"busquedaCuadro";
			this->busquedaCuadro->Size = System::Drawing::Size(345, 87);
			this->busquedaCuadro->TabIndex = 16;
			this->busquedaCuadro->TabStop = false;
			this->busquedaCuadro->Text = L"Filtrar Datos";
			// 
			// campoBusquedaCB
			// 
			this->campoBusquedaCB->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->campoBusquedaCB->FormattingEnabled = true;
			this->campoBusquedaCB->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Nombre", L"Rol" });
			this->campoBusquedaCB->Location = System::Drawing::Point(198, 17);
			this->campoBusquedaCB->Name = L"campoBusquedaCB";
			this->campoBusquedaCB->Size = System::Drawing::Size(141, 21);
			this->campoBusquedaCB->TabIndex = 13;
			// 
			// rolCB
			// 
			this->rolCB->AccessibleRole = System::Windows::Forms::AccessibleRole::None;
			this->rolCB->BackColor = System::Drawing::SystemColors::Window;
			this->rolCB->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->rolCB->FormattingEnabled = true;
			this->rolCB->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"Admin", L"Acceso", L"Gerente" });
			this->rolCB->Location = System::Drawing::Point(79, 105);
			this->rolCB->Name = L"rolCB";
			this->rolCB->Size = System::Drawing::Size(100, 21);
			this->rolCB->TabIndex = 6;
			// 
			// UserSys
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(717, 355);
			this->Controls->Add(this->rolCB);
			this->Controls->Add(this->busquedaCuadro);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->passwordTxBx);
			this->Controls->Add(this->nombreTxBx);
			this->Controls->Add(this->eliminarBtn);
			this->Controls->Add(this->modificarBtn);
			this->Controls->Add(this->agregarBtn);
			this->Controls->Add(this->nuevoBtn);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"UserSys";
			this->Text = L"Administrar Usuarios del sistema";
			this->Load += gcnew System::EventHandler(this, &UserSys::UserSys_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->busquedaCuadro->ResumeLayout(false);
			this->busquedaCuadro->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void actualizarBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		buscarRegistros();
	}		
			 private: System::Void buscarRegistros() {
				 if (busquedaTxBx->Text == "")return;
				 String^ rtn = "getLoginUsers";
				 MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
				 cmd->CommandType = System::Data::CommandType::StoredProcedure;
				 switch (campoBusquedaCB->SelectedIndex)
				 {
				 case 0:
					 cmd->CommandText = "UsersPorNombre";
					 cmd->Parameters->AddWithValue("@inombre", busquedaTxBx->Text);
					 break;
				 case 1:
					 cmd->CommandText = "UsersPorRol";
					 cmd->Parameters->AddWithValue("@irol", busquedaTxBx->Text);
					 break;
				 }
				 MySql::Data::MySqlClient::MySqlDataReader^ myReader = cmd->ExecuteReader();

				 DataTable^ dt = gcnew DataTable();
				 dt->Load(myReader);
				 dataGridView1->DataSource = dt;

				 myReader->Close();
			 }
			 private: System::Void rellenaTabla() {
				 MySql::Data::MySqlClient::MySqlDataAdapter^ sda = gcnew MySql::Data::MySqlClient::MySqlDataAdapter("call getLoginUsers()", conn);
				 DataTable^ dt = gcnew DataTable();
				 sda->Fill(dt);
				 dataGridView1->DataSource = dt;
			 }
private: System::Void dataGridView1_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	int indexRow = e->RowIndex>=0?e->RowIndex:0;
	DataGridViewRow^ rowSelected = dataGridView1->Rows[indexRow];
	nombreTxBx->Text = Convert::ToString(rowSelected->Cells[0]->Value);
	passwordTxBx->Text = Convert::ToString(rowSelected->Cells[1]->Value);
	rolCB->Text = Convert::ToString(rowSelected->Cells[2]->Value);
}
private: System::Void nuevoBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	limpiarCampos();
}
		 private: void limpiarCampos() {
			 nombreTxBx->Text = "";
			 passwordTxBx->Text = "";
			 rolCB->Text = "";
		 }
private: System::Void agregarBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	if (validaCampos())
	{
		Conexion::agregarLoginUser(nombreTxBx->Text, passwordTxBx->Text, rolCB->SelectedItem->ToString(),this->conn);
		rellenaTabla();
	}
}
private: System::Void modificarBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	if (validaCampos())
	{
		Conexion::actualizaLoginUser(nombreTxBx->Text, passwordTxBx->Text, rolCB->SelectedItem->ToString(), this->conn);
		rellenaTabla();
	}
}
private: System::Void eliminarBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	Conexion::eliminarLoginUser(nombreTxBx->Text, this->conn);
	rellenaTabla();
}
private: bool validaCampos() {
	if (revisaVacios())
		if (revisaRol())return true;
		else {
			MessageBox::Show("Seleccione un rol por favor"); return false;
		}
	else MessageBox::Show("No se permite valores vacios");
	return false;
}
private: bool revisaVacios() { return nombreTxBx->Text == "" ? false : passwordTxBx->Text == "" ? false : true; }
private: bool revisaRol() { return rolCB->Text == "Admin" ? true : rolCB->Text == "Acceso" ? true : rolCB->Text == "Gerente" ? true : false; }
private: System::Void UserSys_Load(System::Object^  sender, System::EventArgs^  e) {
	rellenaTabla();
}
private: System::Void busquedaTxBx_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		buscarRegistros();
	}
}
};
}
