#pragma once
#include "Usuario.h"
#include "VehiculoForm.h"
#include "ReporteVehiculo.h"
#include "UserSys.h"
#include "EntradaPeaton.h"
#include "imagen.h"

namespace Formulario {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	/// <summary>
	/// Resumen de ControlAccesoVehicular
	/// </summary>
	public ref class ControlAccesoVehicular : public System::Windows::Forms::Form
	{
	public:         
		ControlAccesoVehicular(MySql::Data::MySqlClient::MySqlConnection^ conn, String^ user, String^ rol)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
			if (rol == "Acceso") {
				this->userSysBtn->Enabled = false;
			}
			this->reportesBtn->Visible = false;
			this->conn = conn;
			this->user = user;
			camaraEntrada->Start();
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~ControlAccesoVehicular()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  userSysBtn;
	private: System::Windows::Forms::Button^  reportesBtn;
	private: System::Windows::Forms::Button^  peatonBtn;

	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::Label^  horaLabel;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  fechaLabel;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::GroupBox^  groupBox1;


	private: String^ user;
	private: int idActual;
	private: MySql::Data::MySqlClient::MySqlConnection^ conn;
	private: System::Windows::Forms::GroupBox^  SessionFields;
	private: System::Windows::Forms::Label^  horaSession;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  fechaSession;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  userSessionL;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Button^  button1;


	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
    private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::CheckBox^  entradaManual;
	private: DateTime^ horayfechas;
	private: bool datosReg = false;
	private: System::Windows::Forms::CheckBox^  salidaManual;
	private: String^ letrasValidas = "abcdefghjklmnprstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ-0987654321";
     private: String^ simbolos = "ABCDEFGHJKLMNPRSTUVWXYZ0123456789-";     

     private:String^ placa = "";
     private: WebCAM::WebCam^  camaraEntrada;
     private: System::Windows::Forms::Button^  button4;
     private: cli::array<double>^ factoresCmpcdad;
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
              this->components = (gcnew System::ComponentModel::Container());
              System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ControlAccesoVehicular::typeid));
              this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
              this->button2 = (gcnew System::Windows::Forms::Button());
              this->button3 = (gcnew System::Windows::Forms::Button());
              this->userSysBtn = (gcnew System::Windows::Forms::Button());
              this->reportesBtn = (gcnew System::Windows::Forms::Button());
              this->peatonBtn = (gcnew System::Windows::Forms::Button());
              this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
              this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
              this->textBox3 = (gcnew System::Windows::Forms::TextBox());
              this->textBox2 = (gcnew System::Windows::Forms::TextBox());
              this->horaLabel = (gcnew System::Windows::Forms::Label());
              this->textBox5 = (gcnew System::Windows::Forms::TextBox());
              this->textBox1 = (gcnew System::Windows::Forms::TextBox());
              this->label7 = (gcnew System::Windows::Forms::Label());
              this->label5 = (gcnew System::Windows::Forms::Label());
              this->label6 = (gcnew System::Windows::Forms::Label());
              this->label1 = (gcnew System::Windows::Forms::Label());
              this->label2 = (gcnew System::Windows::Forms::Label());
              this->label4 = (gcnew System::Windows::Forms::Label());
              this->fechaLabel = (gcnew System::Windows::Forms::Label());
              this->label3 = (gcnew System::Windows::Forms::Label());
              this->textBox4 = (gcnew System::Windows::Forms::TextBox());
              this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
              this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
              this->button4 = (gcnew System::Windows::Forms::Button());
              this->camaraEntrada = (gcnew WebCAM::WebCam());
              this->salidaManual = (gcnew System::Windows::Forms::CheckBox());
              this->entradaManual = (gcnew System::Windows::Forms::CheckBox());
              this->button1 = (gcnew System::Windows::Forms::Button());
              this->SessionFields = (gcnew System::Windows::Forms::GroupBox());
              this->horaSession = (gcnew System::Windows::Forms::Label());
              this->label18 = (gcnew System::Windows::Forms::Label());
              this->fechaSession = (gcnew System::Windows::Forms::Label());
              this->label19 = (gcnew System::Windows::Forms::Label());
              this->userSessionL = (gcnew System::Windows::Forms::Label());
              this->label17 = (gcnew System::Windows::Forms::Label());
              this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
              this->groupBox2->SuspendLayout();
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
              this->groupBox1->SuspendLayout();
              this->SessionFields->SuspendLayout();
              this->SuspendLayout();
              // 
              // dataGridView1
              // 
              this->dataGridView1->AllowUserToAddRows = false;
              this->dataGridView1->AllowUserToDeleteRows = false;
              this->dataGridView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
                  | System::Windows::Forms::AnchorStyles::Right));
              this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
              this->dataGridView1->Location = System::Drawing::Point(803, 22);
              this->dataGridView1->MultiSelect = false;
              this->dataGridView1->Name = L"dataGridView1";
              this->dataGridView1->ReadOnly = true;
              this->dataGridView1->Size = System::Drawing::Size(542, 319);
              this->dataGridView1->TabIndex = 8;
              // 
              // button2
              // 
              this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->button2->FlatAppearance->BorderColor = System::Drawing::Color::Black;
              this->button2->FlatAppearance->BorderSize = 5;
              this->button2->FlatAppearance->MouseDownBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)),
                  static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)));
              this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->button2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button2.Image")));
              this->button2->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
              this->button2->Location = System::Drawing::Point(803, 433);
              this->button2->Name = L"button2";
              this->button2->Size = System::Drawing::Size(137, 47);
              this->button2->TabIndex = 13;
              this->button2->Text = L"Administrar &Vehiculos";
              this->button2->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
              this->button2->UseVisualStyleBackColor = true;
              this->button2->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::button2_Click);
              // 
              // button3
              // 
              this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
              this->button3->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
              this->button3->Location = System::Drawing::Point(803, 370);
              this->button3->Name = L"button3";
              this->button3->Size = System::Drawing::Size(137, 48);
              this->button3->TabIndex = 14;
              this->button3->Text = L"Administrar &Usuarios";
              this->button3->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
              this->button3->UseVisualStyleBackColor = true;
              this->button3->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::button3_Click);
              // 
              // userSysBtn
              // 
              this->userSysBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->userSysBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->userSysBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"userSysBtn.Image")));
              this->userSysBtn->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
              this->userSysBtn->Location = System::Drawing::Point(962, 370);
              this->userSysBtn->Name = L"userSysBtn";
              this->userSysBtn->Size = System::Drawing::Size(137, 47);
              this->userSysBtn->TabIndex = 15;
              this->userSysBtn->Text = L"Usuarios del &Sistema";
              this->userSysBtn->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
              this->userSysBtn->UseVisualStyleBackColor = true;
              this->userSysBtn->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::userSysBtn_Click);
              // 
              // reportesBtn
              // 
              this->reportesBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->reportesBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->reportesBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"reportesBtn.Image")));
              this->reportesBtn->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
              this->reportesBtn->Location = System::Drawing::Point(962, 433);
              this->reportesBtn->Name = L"reportesBtn";
              this->reportesBtn->Size = System::Drawing::Size(137, 50);
              this->reportesBtn->TabIndex = 16;
              this->reportesBtn->Text = L"Administrar &Reportes";
              this->reportesBtn->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
              this->reportesBtn->UseVisualStyleBackColor = true;
              this->reportesBtn->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::reportesBtn_Click);
              // 
              // peatonBtn
              // 
              this->peatonBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->peatonBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->peatonBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"peatonBtn.Image")));
              this->peatonBtn->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
              this->peatonBtn->Location = System::Drawing::Point(1115, 370);
              this->peatonBtn->Name = L"peatonBtn";
              this->peatonBtn->Size = System::Drawing::Size(137, 53);
              this->peatonBtn->TabIndex = 17;
              this->peatonBtn->Text = L"Ingreso &Peatonal";
              this->peatonBtn->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
              this->peatonBtn->UseVisualStyleBackColor = true;
              this->peatonBtn->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::button1_Click);
              // 
              // groupBox2
              // 
              this->groupBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
              this->groupBox2->Controls->Add(this->pictureBox2);
              this->groupBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->groupBox2->Location = System::Drawing::Point(26, 358);
              this->groupBox2->Name = L"groupBox2";
              this->groupBox2->Size = System::Drawing::Size(636, 314);
              this->groupBox2->TabIndex = 29;
              this->groupBox2->TabStop = false;
              this->groupBox2->Text = L"Salida";
              // 
              // pictureBox2
              // 
              this->pictureBox2->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
              this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
              this->pictureBox2->Location = System::Drawing::Point(6, 62);
              this->pictureBox2->Name = L"pictureBox2";
              this->pictureBox2->Size = System::Drawing::Size(291, 204);
              this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
              this->pictureBox2->TabIndex = 25;
              this->pictureBox2->TabStop = false;
              // 
              // textBox3
              // 
              this->textBox3->Enabled = false;
              this->textBox3->Location = System::Drawing::Point(375, 105);
              this->textBox3->Name = L"textBox3";
              this->textBox3->ReadOnly = true;
              this->textBox3->Size = System::Drawing::Size(100, 20);
              this->textBox3->TabIndex = 21;
              // 
              // textBox2
              // 
              this->textBox2->Enabled = false;
              this->textBox2->Location = System::Drawing::Point(376, 74);
              this->textBox2->Name = L"textBox2";
              this->textBox2->ReadOnly = true;
              this->textBox2->Size = System::Drawing::Size(100, 20);
              this->textBox2->TabIndex = 10;
              // 
              // horaLabel
              // 
              this->horaLabel->AutoSize = true;
              this->horaLabel->Location = System::Drawing::Point(545, 16);
              this->horaLabel->Name = L"horaLabel";
              this->horaLabel->Size = System::Drawing::Size(63, 13);
              this->horaLabel->TabIndex = 3;
              this->horaLabel->Text = L"##:##:##";
              // 
              // textBox5
              // 
              this->textBox5->Enabled = false;
              this->textBox5->Location = System::Drawing::Point(375, 164);
              this->textBox5->Name = L"textBox5";
              this->textBox5->ReadOnly = true;
              this->textBox5->Size = System::Drawing::Size(100, 20);
              this->textBox5->TabIndex = 23;
              // 
              // textBox1
              // 
              this->textBox1->AllowDrop = true;
              this->textBox1->Enabled = false;
              this->textBox1->Location = System::Drawing::Point(376, 44);
              this->textBox1->MaxLength = 10;
              this->textBox1->Name = L"textBox1";
              this->textBox1->ReadOnly = true;
              this->textBox1->Size = System::Drawing::Size(100, 20);
              this->textBox1->TabIndex = 9;
              this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &ControlAccesoVehicular::FiltraPlaca);
              // 
              // label7
              // 
              this->label7->AutoSize = true;
              this->label7->Location = System::Drawing::Point(324, 167);
              this->label7->Name = L"label7";
              this->label7->Size = System::Drawing::Size(32, 13);
              this->label7->TabIndex = 20;
              this->label7->Text = L"Tipo";
              // 
              // label5
              // 
              this->label5->AutoSize = true;
              this->label5->Location = System::Drawing::Point(316, 49);
              this->label5->Name = L"label5";
              this->label5->Size = System::Drawing::Size(39, 13);
              this->label5->TabIndex = 5;
              this->label5->Text = L"Placa";
              // 
              // label6
              // 
              this->label6->AutoSize = true;
              this->label6->Location = System::Drawing::Point(304, 77);
              this->label6->Name = L"label6";
              this->label6->Size = System::Drawing::Size(68, 13);
              this->label6->TabIndex = 6;
              this->label6->Text = L"Propietario";
              // 
              // label1
              // 
              this->label1->AutoSize = true;
              this->label1->Location = System::Drawing::Point(502, 16);
              this->label1->Name = L"label1";
              this->label1->Size = System::Drawing::Size(42, 13);
              this->label1->TabIndex = 1;
              this->label1->Text = L"Hora: ";
              // 
              // label2
              // 
              this->label2->AutoSize = true;
              this->label2->Location = System::Drawing::Point(499, 46);
              this->label2->Name = L"label2";
              this->label2->Size = System::Drawing::Size(46, 13);
              this->label2->TabIndex = 2;
              this->label2->Text = L"Fecha:";
              // 
              // label4
              // 
              this->label4->AutoSize = true;
              this->label4->Location = System::Drawing::Point(323, 137);
              this->label4->Name = L"label4";
              this->label4->Size = System::Drawing::Size(36, 13);
              this->label4->TabIndex = 19;
              this->label4->Text = L"Color";
              // 
              // fechaLabel
              // 
              this->fechaLabel->AutoSize = true;
              this->fechaLabel->Location = System::Drawing::Point(545, 46);
              this->fechaLabel->Name = L"fechaLabel";
              this->fechaLabel->Size = System::Drawing::Size(75, 13);
              this->fechaLabel->TabIndex = 4;
              this->fechaLabel->Text = L"__/__/____";
              // 
              // label3
              // 
              this->label3->AutoSize = true;
              this->label3->Location = System::Drawing::Point(318, 105);
              this->label3->Name = L"label3";
              this->label3->Size = System::Drawing::Size(42, 13);
              this->label3->TabIndex = 18;
              this->label3->Text = L"Marca";
              // 
              // textBox4
              // 
              this->textBox4->Enabled = false;
              this->textBox4->Location = System::Drawing::Point(376, 131);
              this->textBox4->Name = L"textBox4";
              this->textBox4->ReadOnly = true;
              this->textBox4->Size = System::Drawing::Size(100, 20);
              this->textBox4->TabIndex = 22;
              // 
              // pictureBox3
              // 
              this->pictureBox3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
              this->pictureBox3->Location = System::Drawing::Point(682, 526);
              this->pictureBox3->Name = L"pictureBox3";
              this->pictureBox3->Size = System::Drawing::Size(372, 151);
              this->pictureBox3->TabIndex = 30;
              this->pictureBox3->TabStop = false;
              // 
              // groupBox1
              // 
              this->groupBox1->Controls->Add(this->button4);
              this->groupBox1->Controls->Add(this->camaraEntrada);
              this->groupBox1->Controls->Add(this->salidaManual);
              this->groupBox1->Controls->Add(this->entradaManual);
              this->groupBox1->Controls->Add(this->button1);
              this->groupBox1->Controls->Add(this->textBox2);
              this->groupBox1->Controls->Add(this->textBox5);
              this->groupBox1->Controls->Add(this->textBox3);
              this->groupBox1->Controls->Add(this->label5);
              this->groupBox1->Controls->Add(this->label1);
              this->groupBox1->Controls->Add(this->label4);
              this->groupBox1->Controls->Add(this->label3);
              this->groupBox1->Controls->Add(this->textBox4);
              this->groupBox1->Controls->Add(this->fechaLabel);
              this->groupBox1->Controls->Add(this->label2);
              this->groupBox1->Controls->Add(this->label6);
              this->groupBox1->Controls->Add(this->label7);
              this->groupBox1->Controls->Add(this->textBox1);
              this->groupBox1->Controls->Add(this->horaLabel);
              this->groupBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
                  static_cast<System::Byte>(0)));
              this->groupBox1->Location = System::Drawing::Point(26, 22);
              this->groupBox1->Name = L"groupBox1";
              this->groupBox1->Size = System::Drawing::Size(636, 319);
              this->groupBox1->TabIndex = 28;
              this->groupBox1->TabStop = false;
              this->groupBox1->Text = L"Entrada";
              // 
              // button4
              // 
              this->button4->Location = System::Drawing::Point(210, 253);
              this->button4->Name = L"button4";
              this->button4->Size = System::Drawing::Size(87, 51);
              this->button4->TabIndex = 37;
              this->button4->Text = L"Capturar Salida";
              this->button4->UseVisualStyleBackColor = true;
              this->button4->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::button4_Click_1);
              // 
              // camaraEntrada
              // 
              this->camaraEntrada->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
              this->camaraEntrada->Imagen = nullptr;
              this->camaraEntrada->Location = System::Drawing::Point(16, 32);
              this->camaraEntrada->MilisegundosCaptura = 100;
              this->camaraEntrada->Name = L"camaraEntrada";
              this->camaraEntrada->Size = System::Drawing::Size(281, 196);
              this->camaraEntrada->TabIndex = 36;
              // 
              // salidaManual
              // 
              this->salidaManual->AutoSize = true;
              this->salidaManual->Location = System::Drawing::Point(505, 107);
              this->salidaManual->Name = L"salidaManual";
              this->salidaManual->Size = System::Drawing::Size(106, 17);
              this->salidaManual->TabIndex = 34;
              this->salidaManual->Text = L"Salida Manual";
              this->salidaManual->UseVisualStyleBackColor = true;
              this->salidaManual->CheckedChanged += gcnew System::EventHandler(this, &ControlAccesoVehicular::salidaManual_CheckedChanged);
              // 
              // entradaManual
              // 
              this->entradaManual->AutoSize = true;
              this->entradaManual->Location = System::Drawing::Point(505, 77);
              this->entradaManual->Name = L"entradaManual";
              this->entradaManual->Size = System::Drawing::Size(115, 17);
              this->entradaManual->TabIndex = 33;
              this->entradaManual->Text = L"Entrada Manual";
              this->entradaManual->UseVisualStyleBackColor = true;
              this->entradaManual->CheckedChanged += gcnew System::EventHandler(this, &ControlAccesoVehicular::entradaManual_CheckedChanged);
              // 
              // button1
              // 
              this->button1->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
              this->button1->Location = System::Drawing::Point(16, 253);
              this->button1->Name = L"button1";
              this->button1->Size = System::Drawing::Size(94, 51);
              this->button1->TabIndex = 32;
              this->button1->Text = L"Capturar Entrada";
              this->button1->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
              this->button1->UseVisualStyleBackColor = true;
              this->button1->Click += gcnew System::EventHandler(this, &ControlAccesoVehicular::button1_Click_1);
              // 
              // SessionFields
              // 
              this->SessionFields->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
              this->SessionFields->Controls->Add(this->horaSession);
              this->SessionFields->Controls->Add(this->label18);
              this->SessionFields->Controls->Add(this->fechaSession);
              this->SessionFields->Controls->Add(this->label19);
              this->SessionFields->Controls->Add(this->userSessionL);
              this->SessionFields->Controls->Add(this->label17);
              this->SessionFields->Location = System::Drawing::Point(1192, 541);
              this->SessionFields->Name = L"SessionFields";
              this->SessionFields->Size = System::Drawing::Size(162, 119);
              this->SessionFields->TabIndex = 31;
              this->SessionFields->TabStop = false;
              this->SessionFields->Text = L"Sesi�n Actual";
              // 
              // horaSession
              // 
              this->horaSession->AutoSize = true;
              this->horaSession->Location = System::Drawing::Point(67, 70);
              this->horaSession->Name = L"horaSession";
              this->horaSession->Size = System::Drawing::Size(65, 13);
              this->horaSession->TabIndex = 5;
              this->horaSession->Text = L"horaSession";
              // 
              // label18
              // 
              this->label18->AutoSize = true;
              this->label18->Location = System::Drawing::Point(7, 70);
              this->label18->Name = L"label18";
              this->label18->Size = System::Drawing::Size(33, 13);
              this->label18->TabIndex = 4;
              this->label18->Text = L"Hora:";
              // 
              // fechaSession
              // 
              this->fechaSession->AutoSize = true;
              this->fechaSession->Location = System::Drawing::Point(67, 45);
              this->fechaSession->Name = L"fechaSession";
              this->fechaSession->Size = System::Drawing::Size(71, 13);
              this->fechaSession->TabIndex = 3;
              this->fechaSession->Text = L"fechaSession";
              // 
              // label19
              // 
              this->label19->AutoSize = true;
              this->label19->Location = System::Drawing::Point(7, 45);
              this->label19->Name = L"label19";
              this->label19->Size = System::Drawing::Size(40, 13);
              this->label19->TabIndex = 2;
              this->label19->Text = L"Fecha:";
              // 
              // userSessionL
              // 
              this->userSessionL->AutoSize = true;
              this->userSessionL->Location = System::Drawing::Point(78, 20);
              this->userSessionL->Name = L"userSessionL";
              this->userSessionL->Size = System::Drawing::Size(78, 13);
              this->userSessionL->TabIndex = 1;
              this->userSessionL->Text = L"usuarioSession";
              // 
              // label17
              // 
              this->label17->AutoSize = true;
              this->label17->Location = System::Drawing::Point(7, 20);
              this->label17->Name = L"label17";
              this->label17->Size = System::Drawing::Size(65, 13);
              this->label17->TabIndex = 0;
              this->label17->Text = L"Iniciada por:";
              // 
              // timer1
              // 
              this->timer1->Interval = 1000;
              this->timer1->Tick += gcnew System::EventHandler(this, &ControlAccesoVehicular::timer1_Tick);
              // 
              // ControlAccesoVehicular
              // 
              this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
              this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
              this->ClientSize = System::Drawing::Size(1357, 684);
              this->Controls->Add(this->SessionFields);
              this->Controls->Add(this->pictureBox3);
              this->Controls->Add(this->dataGridView1);
              this->Controls->Add(this->groupBox2);
              this->Controls->Add(this->button3);
              this->Controls->Add(this->groupBox1);
              this->Controls->Add(this->button2);
              this->Controls->Add(this->userSysBtn);
              this->Controls->Add(this->reportesBtn);
              this->Controls->Add(this->peatonBtn);
              this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
              this->Name = L"ControlAccesoVehicular";
              this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
              this->Text = L"ControlAccesoVehicular";
              this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ControlAccesoVehicular::ControlAccesoVehicular_FormClosing);
              this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &ControlAccesoVehicular::ControlAccesoVehicular_FormClosed);
              this->Load += gcnew System::EventHandler(this, &ControlAccesoVehicular::ControlAccesoVehicular_Load);
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
              this->groupBox2->ResumeLayout(false);
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
              (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
              this->groupBox1->ResumeLayout(false);
              this->groupBox1->PerformLayout();
              this->SessionFields->ResumeLayout(false);
              this->SessionFields->PerformLayout();
              this->ResumeLayout(false);

          }
#pragma endregion
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	Formulario::VehiculoForm^ ventanaVehiculo = gcnew VehiculoForm(this->conn);
	ventanaVehiculo->ShowDialog();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	Formulario::Usuario^ ventanaUsuario = gcnew Usuario(this->conn);
	ventanaUsuario->ShowDialog();
}
private: System::Void ControlAccesoVehicular_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
	//Application::Exit();
}
private: System::Void reportesBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	Formulario::ReporteVehiculo^ ventanaReportes = gcnew ReporteVehiculo();
	ventanaReportes->ShowDialog();
}
private: System::Void userSysBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	Formulario::UserSys^ ventanaUsuarioS = gcnew UserSys(this->conn);
	ventanaUsuarioS->ShowDialog();
}
private: System::Void peatonBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	Formulario::EntradaPeaton^ ventanaPeaton = gcnew EntradaPeaton();
	ventanaPeaton->ShowDialog();
}
private: System::Void ControlAccesoVehicular_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	//Cerrar sesion
	Conexion::actualizaHoraSalidaSistema(idActual, conn);
}

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	Formulario::EntradaPeaton^ ventanaPeaton = gcnew EntradaPeaton();
	ventanaPeaton->ShowDialog();
}

private: System::Void panel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}     
private: System::Void ControlAccesoVehicular_Load(System::Object^  sender, System::EventArgs^  e) {
	idActual = Conexion::iniciarSistema(user, conn);
	horayfechas = Conexion::getFechaSistema(conn);
	userSessionL->Text = user;
	horaSession->Text = horayfechas->ToShortTimeString();
	fechaSession->Text = horayfechas->ToShortDateString();
	horaLabel->Text = horayfechas->ToLongTimeString();
	fechaLabel->Text = horayfechas->ToShortDateString();
	rellenaTabla();
	timer1->Start();
    camaraEntrada->Start();
    camaraEntrada->Start();
	entrenarCompacidad(false);
}
private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {	    
    procesaImagenCamara();
}

private:
    Bitmap^ image1;
Color newColor;
cli::array<cli::array<int,2>^>^objetos;//en este array hay un array bidimensinal el cual va a servir para sacar el area y el perimetro y despues su compasidad
cli::array<int, 2>^ arre;//arreglo original de la imagen binaria
int etiquetas = 0;
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {  
 
}
         private: int menorDe(cli::array<double>^ arreglo) {
             int pos = 0;
             double menor = arreglo[0];
             for (int i = 1; i < arreglo->GetLength(0); i++) {
                 if (arreglo[i] < menor) {
                     pos = i;
                     menor = arreglo[i];
                 }
             }
             return pos;
         }
         private:System::Void escalaGrises() {
             try
             {
                 image1 = gcnew Bitmap(pictureBox2->Image);//"C:\\Users\\Alberto\\Pictures\\monta�a.png", true);

                 int x;
                 int y;
                 //Console::WriteLine(image1->Width + " " + image1->Height);
                 for (x = 0; x < image1->Width; x++)
                 {
                     for (y = 0; y < image1->Height; y++)
                     {
                         Color pixelColor = image1->GetPixel(x, y);
                         int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                         Color newColor = Color::FromArgb(valor, valor, valor);
                         image1->SetPixel(x, y, newColor);


                     }

                 }

                 pictureBox2->Image = image1;

             }
             catch (ArgumentException^)
             {
                 MessageBox::Show("Error al cargar la imagen");
             }
         }
        private:System::Void blancoYNegro() {            
            try
            {
                image1 = gcnew Bitmap(pictureBox2->Image);//"C:\\Users\\Alberto\\Pictures\\monta�a.png", true);        
                int x;
                int y;
                for (x = 0; x < image1->Height; x++)
                {
                    for (y = 0; y < image1->Width; y++)
                    {

                        Color pixelColor = image1->GetPixel(y, x);

                        int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                        if (valor >= 0 && valor <= 50)
                        {
                            newColor = Color::FromArgb(0, 0, 0);
                            arre[x,y] = 1;
                        }
                        else
                        {
                            newColor = Color::FromArgb(255, 255, 255);
                            arre[x,y] = 0;
                        }

                        image1->SetPixel(y, x, newColor);
                    }
                }
                pictureBox2->Image = image1;


            }
            catch (ArgumentException^)
            {
                MessageBox::Show("Error al cargar la imagen");
            }
           
        }
        private:System::Void asignaEtiquetasBarrido1() {//barrido de arriba-abajo izq derecha
            int contador = 1;
            for (int i = 1; i < image1->Height - 1; i++) {
                for (int j = 1; j < image1->Width - 1; j++) {
                    if (arre[i,j] == 1) {
                        bool vecino = false;
                        if (arre[i - 1,j - 1] != 0) {//busca vecino diag-izq
                            vecino = true;
                            arre[i,j] = arre[i - 1,j - 1];
                        }
                        if (arre[i - 1,j] != 0) {//busca vecino superior
                            vecino = true;
                            arre[i,j] = arre[i - 1,j];
                        }
                        if (arre[i - 1,j + 1] != 0) {//busca vecino diag-der
                            vecino = true;
                            arre[i,j] = arre[i - 1,j + 1];
                        }
                        if (arre[i,j - 1] != 0) {//busca vecino izq
                            vecino = true;
                            arre[i,j] = arre[i,j - 1];
                        }
                        if (vecino == false) {//agrega una nueva etiqueta                            
                            arre[i,j] = contador;
                            contador += 1;
                        }
                    }
                }
            }
            etiquetas = contador;
        }
        private:System::Void asignaEtiquetasBarrido2() {
          // Console::WriteLine("" + image1->Height + "" +image1->Width);
            for (int i = image1->Height-2; i > 0; i--) {
                for (int j = image1->Width-2; j > 0; j--) {
                    if (arre[i,j] != 0) {
                        bool vecino = false;
                        if (arre[i + 1,j + 1] != 0) {//busca vecino diag-der
                            vecino = true;
                            arre[i,j] = arre[i + 1,j + 1];
                        }
                        if (arre[i + 1,j] != 0) {//busca vecino inferior
                            vecino = true;
                            arre[i,j] = arre[i + 1,j];
                        }
                        if (arre[i + 1,j - 1] != 0) {//busca vecino diag-izq
                            vecino = true;
                            arre[i,j] = arre[i + 1,j - 1];
                        }
                        if (arre[i,j + 1] != 0) {//busca vecino der
                            vecino = true;
                            arre[i,j] = arre[i,j + 1];
                        }                        
                    }
                }
            }
        }
        private:System::Void imprime(cli::array<int, 2>^ arre) {          
            for (int i = 0; i <image1->Height; i++) {
                for (int j = 0; j <image1->Width; j++) {
                    Console::Write(arre[i,j]);
                }
                Console::WriteLine();
            }
        }
        private:System::Void separaObjetosImagen() {//este metodo extrae cada objeto etiquetado y lo guarda en el arreglo que almacena arreglos bidimensionales
            int etiqueta=1;
            objetos = gcnew cli::array<cli::array<int,2>^>(etiquetas);
            cli::array<int, 2>^objeto;
            while (etiqueta!=etiquetas)//ciclo que separa todos los objetos en arreglo bidimensionales de acuerdo a la cantidad de etiquetas
            {
                objeto = gcnew cli::array<int, 2>(pictureBox2->Image->Height, pictureBox2->Image->Width);
                for (int i = 0; i < image1->Height; i++) {
                    for (int j = 0; j < image1->Width; j++) {
                        if (arre[i, j] == etiqueta) {
                            objeto[i, j] = arre[i, j];
                        }
                        else {
                            objeto[i, j] =0;
                        }
                    }                    
                    objetos[etiqueta - 1] = objeto;                     
                }
                etiqueta++;
				Area(objeto);
				
            }
        }
        private:System::Void imprimeObjetos() {
            int index = 0;
            //while (index!=objetos->Length)
            //{
                Console::WriteLine("Obj"+index);
                imprime(objetos[3]);
              //  index++;
            //}
        }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	horayfechas = horayfechas->AddSeconds(1);
	horaLabel->Text = horayfechas->ToLongTimeString();
	fechaLabel->Text = horayfechas->ToShortDateString();
	timer1->Start();
}
private: System::Void entradaManual_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	if (entradaManual->Checked == true)
	{
		textBox1->Enabled = true;
		textBox1->ReadOnly = false;
	}
	else
	{
		textBox1->Enabled = false;
		textBox1->ReadOnly = true;
		resetMetodoManual(true);
	}
}
private: System::Void FiltraPlaca(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		accionManual();
		e->Handled = true;
		return;
	}
	else if (letrasValidas->Contains(Convert::ToString(e->KeyChar)) || e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Back)) {
		//Nada
	}
	else {
		e->Handled = true;
	}
	resetMetodoManual(false);
}
private: System::Void accionManual() {
	if (!datosReg)
	{
		textBox1->Text = textBox1->Text->ToUpper();
		obtenDatosPlaca();
		datosReg = true;
	}
	else
	{
		if (entradaManual->Checked)
		{
			insertaEntradaVehiculo();
		}
		else if (salidaManual->Checked)
		{
			insertaSalidaVehiculo();
		}
		entradaManual->Checked = false;
		salidaManual->Checked = false;
		datosReg = false;
		rellenaTabla();
		
	}
}
private: System::Void resetMetodoManual(bool full) {
	datosReg = false;
	if (full) textBox1->Text = "";
	textBox2->Text = "";
	textBox3->Text = "";
	textBox4->Text = "";
	textBox5->Text = "";
}
private: System::Void insertaSalidaVehiculo() {
	try {
		String^ rtn = "registrarSalidaVehiculo";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@imatricula", textBox1->Text);
		int result = cmd->ExecuteNonQuery();
		if (result == 1)//MessageBox::Show("Insertado Correctamente");
			Console::WriteLine("Salida Vehicular Exitosa");
	}
	catch (Exception^ ex)
	{
		MessageBox::Show("No se ha podido registrar Salida\n Mensaje de error:\n" + ex->Message);
	}
}
private: System::Void insertaEntradaVehiculo() {
	try {
		String^ rtn = "registrarEntradaVehiculo";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@imatricula", textBox1->Text);
		int result = cmd->ExecuteNonQuery();
		if (result == 1)//MessageBox::Show("Insertado Correctamente");
			Console::WriteLine("Ingreso Vehicular Exitoso");
	}
	catch (Exception^ ex)
	{
		MessageBox::Show("No se ha podido registrar entrada\n Mensaje de error:\n"+ex->Message);
	}
}
private: System::Void obtenDatosPlaca() {
	try{
		String^ rtn = "obtenDatosPlaca";
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand(rtn, conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;
		cmd->Parameters->AddWithValue("@iplaca", textBox1->Text);
		MySql::Data::MySqlClient::MySqlDataReader^ myReader = cmd->ExecuteReader();
		while (myReader->Read())
		{
			textBox2->Text = myReader->GetString(0);
			textBox3->Text = myReader->GetString(1);
			textBox4->Text = myReader->GetString(2);
			textBox5->Text = myReader->GetString(3);
		}
		myReader->Close();
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}
		 int k = 0;
		// int l = 1;
private: System::Void prueba() {
	cli::array<String^>^ numeros = gcnew cli::array<String^>(11) { "CERO", "UNO", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", "guion" };
	String^ nombre = "../ImagenesLetras/" + numeros[k] + 3 + ".png";
	Console::WriteLine(nombre);
	
		Bitmap^ simbolo = gcnew Bitmap(nombre);
		Bitmap^ simboloGrises = escalaGrises(simbolo);
		Bitmap^ simboloBinario = blancoYNegro(simboloGrises);
		//cli::array<int, 2>^ matrizSimbolo = matrizDeImagen(simboloBinario);
		pictureBox2->Image = simboloBinario;
		k++;
}
private: System::Void entrenarCompacidad(bool f) {
	factoresCmpcdad = gcnew cli::array<double>(34);
	if (f) {
		String^ letras = "abcdefghjklmnprstuvwxyz";
		cli::array<String^>^ numeros = gcnew cli::array<String^>(11) { "CERO", "UNO", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", "guion" };
		for (int i = 0; i < letras->Length; i++)
		{
			double compacidadAc = 0;
			for (int j = 1; j < 4; j++) {
				String^ nombre = "../ImagenesLetras/" + letras->ToCharArray()[i] + j + ".png";
				Console::WriteLine(nombre);
				try {
					Bitmap^ simbolo = gcnew Bitmap(nombre);
					Bitmap^ simboloGrises = escalaGrises(simbolo);
					Bitmap^ simboloBinario = blancoYNegro(simboloGrises);
					cli::array<int, 2>^ matrizSimbolo = matrizDeImagen(simboloBinario);
					//etiquetado 
					long areaSimbolo = Area(matrizSimbolo);
					long perimSimbolo = Perimetro(matrizSimbolo);
					double compacidadSimbolo = compacidad(areaSimbolo, perimSimbolo);
					compacidadAc += compacidadSimbolo;
				}
				catch (Exception^ e) { MessageBox::Show(e->Message); }
			}
			factoresCmpcdad[i] = (compacidadAc / 3);
		}
		for (int i = 0; i < numeros->GetLength(0); i++)
		{
			double compacidadAc = 0;
			for (int j = 1; j < 4; j++)
			{
				String^ nombre = "../ImagenesLetras/" + numeros[i] + j + ".png";
				Console::WriteLine(nombre);
				Bitmap^ simbolo = gcnew Bitmap(nombre);
				Bitmap^ simboloGrises = escalaGrises(simbolo);
				Bitmap^ simboloBinario = blancoYNegro(simboloGrises);
				cli::array<int, 2>^ matrizSimbolo = matrizDeImagen(simboloBinario);
				//etiquetado 
				long areaSimbolo = Area(matrizSimbolo);
				long perimSimbolo = Perimetro(matrizSimbolo);
				double compacidadSimbolo = compacidad(areaSimbolo, perimSimbolo);
				compacidadAc += compacidadSimbolo;
			}
			factoresCmpcdad[23 + i] = (compacidadAc / 3);
		}
		try
		{
			//Pass the file path and file name to the StreamWriter Constructor.
			System::IO::StreamWriter^ sw = gcnew System::IO::StreamWriter("./compacidad.txt");
			//Write a line of text.
			for (int i = 0; i < factoresCmpcdad->GetLength(0); i++)
			{
				Console::WriteLine(factoresCmpcdad[i]);
				sw->WriteLine(factoresCmpcdad[i]);
			}
			//Close the file.
			sw->Close();
		}
		catch (Exception^ e)
		{
			Console::WriteLine("Exception: {0}", e->Message);
		}
	}
	else {
		//leer del archivo

		try
		{
			//Pass the file path and file name to the StreamReader constructor.
			System::IO::StreamReader^ sr = gcnew System::IO::StreamReader("./compacidad.txt");
			String^ cadena;
			Console::WriteLine("Si hay pero ocurre un error");
			for (int i = 0; i < factoresCmpcdad->GetLength(0); i++) {
				cadena = sr->ReadLine();
				Console::WriteLine(cadena);
				factoresCmpcdad[i] = Convert::ToDouble(cadena);
			}
			//Close the file.
			sr->Close();
		}
		catch (Exception^ e)
		{
			Console::WriteLine("No hay compacidad almacenada, se mandar� a generar");
			entrenarCompacidad(true);
		}		
	}
}
		private: double compacidad(long area, long perimetro) {
			double calculo = 0.282*perimetro / Math::Sqrt(area);
			Console::WriteLine("Compacidad::{0}",calculo);
			return calculo;
		}
		 private: Bitmap^ escalaGrises(Bitmap^ imagenEntrada) {
			 Bitmap^ imagen = gcnew Bitmap(imagenEntrada);
			 try
			 {
				 int x;
				 int y;
				 for (x = 0; x < imagen->Width; x++)
				 {
					 for (y = 0; y < imagen->Height; y++)
					 {
						 Color pixelColor = imagen->GetPixel(x, y);
						 int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
						 Color newColor = Color::FromArgb(valor, valor, valor);
						 imagen->SetPixel(x, y, newColor);
					 }
				 }
				 return imagen;
			 }
			 catch (ArgumentException^)
			 {
				 MessageBox::Show("Error al cargar la imagen");
			 }
		 }
	 private: cli::array<int, 2>^ matrizDeImagen(Bitmap^ imagenEntrada) {
		 cli::array<int, 2>^ matrizSalida = gcnew cli::array<int, 2>(imagenEntrada->Height, imagenEntrada->Width);
		 try
		 {
			 int x;
			 int y;
			 for (x = 0; x < imagenEntrada->Height; x++)
			 {
				 for (y = 0; y < imagenEntrada->Width; y++)
				 {
					 Color pixelColor = imagenEntrada->GetPixel(y, x);
					 int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
					 if (valor ==0)
					 {
						 matrizSalida[x, y] = 1;
					 }
					 else
					 {
						 matrizSalida[x, y] = 0;
					 }
				 }
			 }
			 return matrizSalida;
		 }
		 catch (ArgumentException^)
		 {
			 MessageBox::Show("Error al cargar la imagen");
		 }
	 }
	 private: Bitmap^ blancoYNegro(Bitmap^ imagenEntrada) {
		 Bitmap^ imagen = gcnew Bitmap(imagenEntrada);
		 try
		 {
			 int x;
			 int y;
			 for (x = 0; x < imagen->Height; x++)
			 {
				 for (y = 0; y < imagen->Width; y++)
				 {
					 Color pixelColor = imagen->GetPixel(y, x);
					 int valor = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
					 if (valor >= 0 && valor <= 50)
					 {
						 newColor = Color::FromArgb(0, 0, 0);
						 //arre[x, y] = 1;
					 }
					 else
					 {
						 newColor = Color::FromArgb(255, 255, 255);
						 //arre[x, y] = 0;
					 }
					 imagen->SetPixel(y, x, newColor);
				 }
			 }
			 return imagen;
		 }
		 catch (ArgumentException^)
		 {
			 MessageBox::Show("Error al cargar la imagen");
		 }

	 }
		 private: long Area(cli::array<int, 2>^ arre)
		 {
			 long area = 0;
			 for (int i = 0; i <arre->GetLength(0); i++) {
				 for (int j = 0; j <arre->GetLength(1); j++) {
					 if (arre[i, j] != 0)
					 {
						 area++;
					 }
				 }
			 }
			 Console::WriteLine("area:  "+ area);
			 return area;
		 }
		private: long Perimetro(cli::array<int, 2>^ arre)
		{
			long perimetro = 0;
			for (int i = 1; i < arre->GetLength(0) - 1; i++) {
				for (int j = 1; j < arre->GetLength(1) - 1; j++) {
					if (arre[i, j] != 0) {
						if (arre[i - 1, j] == 0) {//busca vecino superior
							perimetro++;
						}
						if (arre[i, j - 1] == 0) {//busca vecino izq
							perimetro++;
						}
						if (arre[i, j + 1] == 0) {//busca vecino derecho
							perimetro++;
						}

						if (arre[i + 1, j] == 0) {//busca vecino inferiro
							perimetro++;
						}
					}
				}
			}
			Console::WriteLine("perimetro:  " + perimetro);			
			return perimetro;
		}
private: double distancia(double x1, double y1, double x2, double y2) {
	double distancia = 0;
	distancia = Math::Sqrt(Math::Pow((x2 - x1), 2) - Math::Pow((y2 - y1), 2));
	return distancia;
}
private: System::Void rellenaTabla() {
	MySql::Data::MySqlClient::MySqlDataAdapter^ sda = gcnew MySql::Data::MySqlClient::MySqlDataAdapter("call getEntradasVehiculosHoy()", conn);
	DataTable^ dt = gcnew DataTable();
	sda->Fill(dt);
	dataGridView1->DataSource = dt;
}
private: System::Void salidaManual_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	if (salidaManual->Checked == true)
	{
		textBox1->Enabled = true;
		textBox1->ReadOnly = false;
	}
	else
	{
		textBox1->Enabled = false;
		textBox1->ReadOnly = true;
		resetMetodoManual(true);
	}
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	prueba();
}
private: System::Void button4_Click_1(System::Object^  sender, System::EventArgs^  e) {
    procesaImagenCamara();
    insertaSalidaVehiculo();
}
         private:System::Void procesaImagenCamara() {
             pictureBox2->Image = camaraEntrada->GetImage();
             escalaGrises();
             // Console::WriteLine("" + pictureBox2->Image->Height + "" + pictureBox2->Image->Width);
             arre = gcnew cli::array<int, 2>(pictureBox2->Image->Height, pictureBox2->Image->Width);
             blancoYNegro();
             //barrido1
             asignaEtiquetasBarrido1();
             //finbarrido1
             //barrido2    
             asignaEtiquetasBarrido2();
             //fin barrido2
             //imprimir array
             imprime(arre);
             //Perimetro(arre);
             separaObjetosImagen();
             String^ placaIdentificada = "";
             for (int i = 0; i < objetos->GetLength(0); i++) {
                 if (objetos[i] != nullptr)
                 {
                     long area = Area(objetos[i]);
                     Console::WriteLine("obj {0}::{1}", i, area);
                     long perimetro = 0;
                     if (area != 0)
                     {
                         perimetro = Perimetro(objetos[i]);
                         long compac = compacidad(area, perimetro);
                         cli::array<double>^ distancias = gcnew cli::array<double>(34);
                         for (int j = 0; j < 34; j++) {
                             distancias[j] = Math::Abs(factoresCmpcdad[j] - compac);
                         }
                         int menor = menorDe(distancias);
                         Console::WriteLine("Valor identificado::{0}", simbolos->ToCharArray()[menor]);
                         placaIdentificada += simbolos->ToCharArray()[menor];
                     }
                 }
             }
             textBox1->Text = placaIdentificada;
         }
};
}
