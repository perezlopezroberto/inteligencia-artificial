#pragma once
#include "iostream"
#include "ctime"
#include "windows.h"
#include "Conexion.h"

namespace Formulario {


	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;
   

	/// <summary>
	/// Resumen de EntradaPeaton
	/// </summary>
	public ref class EntradaPeaton : public System::Windows::Forms::Form
	{
	public:
		EntradaPeaton(void)
		{
			//button3->Enabled = false;
			InitializeComponent();
			
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~EntradaPeaton()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  fechaLabel;
	private: System::Windows::Forms::Label^  horaLabel;
	private: System::Windows::Forms::Timer^  timer1;








	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::DataGridView^  dataGridView2;
	private: System::Windows::Forms::Button^  button2;
	String^ folio="";
	private: System::Windows::Forms::Button^  button3;

	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(EntradaPeaton::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->fechaLabel = (gcnew System::Windows::Forms::Label());
			this->horaLabel = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->dataGridView2 = (gcnew System::Windows::Forms::DataGridView());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(14, 58);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(67, 16);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Nombre;";
			this->label1->Click += gcnew System::EventHandler(this, &EntradaPeaton::label1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(12, 94);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(128, 16);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Apellido Paterno:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(12, 129);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(130, 16);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Apellido Materno:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(14, 209);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(55, 16);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Fecha:";
			this->label4->Click += gcnew System::EventHandler(this, &EntradaPeaton::label4_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(14, 246);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(104, 16);
			this->label5->TabIndex = 4;
			this->label5->Text = L"Hora Entrada:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(380, 20);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(57, 16);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Salida:";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(156, 54);
			this->textBox1->MaxLength = 45;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(194, 20);
			this->textBox1->TabIndex = 6;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &EntradaPeaton::textBox1_TextChanged);
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EntradaPeaton::textBox1_KeyPress);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(156, 90);
			this->textBox2->MaxLength = 45;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(194, 20);
			this->textBox2->TabIndex = 7;
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &EntradaPeaton::textBox2_TextChanged);
			this->textBox2->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EntradaPeaton::textBox2_KeyPress);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(156, 125);
			this->textBox3->MaxLength = 45;
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(194, 20);
			this->textBox3->TabIndex = 8;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &EntradaPeaton::textBox3_TextChanged);
			this->textBox3->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EntradaPeaton::textBox3_KeyPress);
			// 
			// fechaLabel
			// 
			this->fechaLabel->AutoSize = true;
			this->fechaLabel->Location = System::Drawing::Point(153, 211);
			this->fechaLabel->Name = L"fechaLabel";
			this->fechaLabel->Size = System::Drawing::Size(65, 13);
			this->fechaLabel->TabIndex = 9;
			this->fechaLabel->Text = L"__/__/____";
			this->fechaLabel->Click += gcnew System::EventHandler(this, &EntradaPeaton::fechaLabel_Click);
			// 
			// horaLabel
			// 
			this->horaLabel->AutoSize = true;
			this->horaLabel->Location = System::Drawing::Point(153, 248);
			this->horaLabel->Name = L"horaLabel";
			this->horaLabel->Size = System::Drawing::Size(55, 13);
			this->horaLabel->TabIndex = 10;
			this->horaLabel->Text = L"##:##:##";
			this->horaLabel->Click += gcnew System::EventHandler(this, &EntradaPeaton::horaLabel_Click);
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &EntradaPeaton::timer1_Tick);
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button1->Location = System::Drawing::Point(243, 220);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(107, 42);
			this->button1->TabIndex = 12;
			this->button1->Text = L"Agregar";
			this->button1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &EntradaPeaton::button1_Click);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label8->Location = System::Drawing::Point(12, 169);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(59, 16);
			this->label8->TabIndex = 14;
			this->label8->Text = L"Asunto:";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(156, 165);
			this->textBox4->MaxLength = 40;
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(194, 20);
			this->textBox4->TabIndex = 15;
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &EntradaPeaton::textBox4_TextChanged);
			this->textBox4->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EntradaPeaton::textBox4_KeyPress);
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(451, 16);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(194, 20);
			this->textBox5->TabIndex = 16;
			this->textBox5->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EntradaPeaton::textBox5_KeyPress);
			// 
			// dataGridView2
			// 
			this->dataGridView2->AllowUserToAddRows = false;
			this->dataGridView2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView2->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView2->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView2->Location = System::Drawing::Point(383, 54);
			this->dataGridView2->MultiSelect = false;
			this->dataGridView2->Name = L"dataGridView2";
			this->dataGridView2->ReadOnly = true;
			this->dataGridView2->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView2->Size = System::Drawing::Size(503, 131);
			this->dataGridView2->TabIndex = 18;
			this->dataGridView2->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &EntradaPeaton::dataGridView2_CellClick);
			this->dataGridView2->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &EntradaPeaton::dataGridView2_CellContentClick);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button2->Location = System::Drawing::Point(666, 7);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(96, 42);
			this->button2->TabIndex = 19;
			this->button2->Text = L"Buscar";
			this->button2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &EntradaPeaton::button2_Click);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
			this->button3->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button3->Location = System::Drawing::Point(779, 7);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(107, 42);
			this->button3->TabIndex = 20;
			this->button3->Text = L"Agregar";
			this->button3->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &EntradaPeaton::button3_Click);
			// 
			// EntradaPeaton
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(898, 374);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->dataGridView2);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->horaLabel);
			this->Controls->Add(this->fechaLabel);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"EntradaPeaton";
			this->Text = L"EntradaPeaton";
			this->Load += gcnew System::EventHandler(this, &EntradaPeaton::EntradaPeaton_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label4_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void fechaLabel_Click(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void horaLabel_Click(System::Object^  sender, System::EventArgs^  e) {
	
}
private: System::Void EntradaPeaton_Load(System::Object^  sender, System::EventArgs^  e) {

	button3->Enabled = false;
    
	time_t now = time(0);
	tm *ltm = localtime(&now);
	 
	int aux = ltm->tm_mday;
	int aux1 = 1 + ltm->tm_mon;

	if (aux < 10) {
		if (aux1<10) {
			this->fechaLabel->Text = "0" + aux + "/" + "0" + aux1 + "/" + (1900 + ltm->tm_year);
		}
		else {
			this->fechaLabel->Text = "0" + aux + "/" + aux1 + "/" + (1900 + ltm->tm_year);
		}
	}
	else {
		if (aux1<10) {
			this->fechaLabel->Text = aux + "/" + "0" + aux1 + "/" + (1900 + ltm->tm_year);
		}
		else {
			this->fechaLabel->Text = aux + "/" + aux1 + "/" + (1900 + ltm->tm_year);
		}
	}
	timer1->Start();
	DateTime datetime = DateTime::Now;
	this->horaLabel->Text = datetime.ToLongTimeString();

}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	DateTime datetime = DateTime::Now;
	this->horaLabel->Text = datetime.ToLongTimeString();
	timer1->Start();
}
private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	Conexion con = Conexion();
	if (textBox1->Text == "")
	{
		MessageBox::Show("Rellene el campo Nombre por favor");
		return;
	}
	if (textBox2->Text == "")
	{
		MessageBox::Show("Rellene el campo Apellido Paterno por favor");
		return;
	}
	if (textBox4->Text == "")
	{
		MessageBox::Show("Rellene el campo Asunto por favor");
		return;
	}
	try {

		time_t now = time(0);
		tm *ltm = localtime(&now);

		int aux = ltm->tm_mday;
		int aux1 = 1 + ltm->tm_mon;

		String^ horaEntrada = "";

		DateTime datetime = DateTime::Now;

		if (aux < 10) {
			if (aux1<10) {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "0" + aux1 + "-" + "0" + aux + " " + datetime.ToString("HH:mm:ss");
			}
			else {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "" + aux1 + "-" + "0" + aux + " " + datetime.ToString("HH:mm:ss");
			}
		}
		else {
			if (aux1<10) {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "0" + aux1 + "-" + "" + aux + " " + datetime.ToString("HH:mm:ss");
			}
			else {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "0" + aux1 + "-" + "0" + aux + " " + datetime.ToString("HH:mm:ss");
			}
		}

		String^ nombre = textBox1->Text;
		String^ apaterno = textBox2->Text;
		String^ amaterno = textBox3->Text;
		String^ motivoVista = textBox4->Text;

		con.InsertarPeatonE(nombre, apaterno, amaterno, horaEntrada, motivoVista);

		textBox1->Text = "";
		textBox2->Text = "";
		textBox3->Text = "";
		textBox4->Text = "";
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}
private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {

}
private: System::Void textBox4_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {

	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		revisarLogin();
	}

}
private: System::Void revisarLogin() {
	Conexion con = Conexion();
	if (textBox1->Text == "")
	{
		textBox1->Focus();
		MessageBox::Show("Rellene el campo Nombre por favor");
		return;
	}
	if (textBox2->Text == "")
	{
		textBox2->Focus();
		MessageBox::Show("Rellene el campo Apellido Paterno por favor");
		return;
	}
	if (textBox4->Text == "")
	{
		textBox4->Focus();
		MessageBox::Show("Rellene el campo Asunto por favor");
		return;
	}
	try {

		time_t now = time(0);
		tm *ltm = localtime(&now);

		int aux = ltm->tm_mday;
		int aux1 = 1 + ltm->tm_mon;

		String^ horaEntrada = "";

		DateTime datetime = DateTime::Now;

		if (aux < 10) {
			if (aux1<10) {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "0" + aux1 + "-" + "0" + aux + " " + datetime.ToString("HH:mm:ss");
			}
			else {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "" + aux1 + "-" + "0" + aux + " " + datetime.ToString("HH:mm:ss");
			}
		}
		else {
			if (aux1<10) {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "0" + aux1 + "-" + "" + aux + " " + datetime.ToString("HH:mm:ss");
			}
			else {
				horaEntrada = (1900 + ltm->tm_year) + "-" + "0" + aux1 + "-" + "0" + aux + " " + datetime.ToString("HH:mm:ss");
			}
		}

		String^ nombre = textBox1->Text;
		String^ apaterno = textBox2->Text;
		String^ amaterno = textBox3->Text;
		String^ motivoVista = textBox4->Text;

		con.InsertarPeatonE(nombre, apaterno, amaterno, horaEntrada, motivoVista);

		textBox1->Text = "";
		textBox2->Text = "";
		textBox3->Text = "";
		textBox4->Text = "";
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}

private: System::Void textBox3_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		revisarLogin();
	}
}
private: System::Void textBox2_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		revisarLogin();
	}
}
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void textBox1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		revisarLogin();
	}
}
private: System::Void dataGridView1_CellContentClick_1(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	BuscaNombre();
}
private: System::Void BuscaNombre() {

	if (textBox5->Text == "")
	{
		textBox5->Focus();
		MessageBox::Show("Rellene el campo");
		return;
	}

	String^ busc = textBox5->Text;
	Conexion c = Conexion::Conexion();
	String^ constr = c.getStringConexion();
	DataTable^ dt = gcnew DataTable();
	MySql::Data::MySqlClient::MySqlConnection^ con = gcnew MySql::Data::MySqlClient::MySqlConnection(constr);
	con->Open();
	MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand("", con);
	cmd->CommandType = System::Data::CommandType::StoredProcedure;

	cmd->CommandText = "buscarentradapeaton";
	cmd->Parameters->AddWithValue("@inombre", busc);

	MySql::Data::MySqlClient::MySqlDataReader^ myReader = cmd->ExecuteReader();
	//DataTable^ dt = gcnew DataTable();
	dt->Load(myReader);
	dataGridView2->DataSource = dt;
	myReader->Close();
	con->Close();
}
private: System::Void dataGridView2_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	int indexRow = e->RowIndex >= 0 ? e->RowIndex : 0;
	DataGridViewRow^ rowSelected = dataGridView2->Rows[indexRow];
	folio = Convert::ToString(rowSelected->Cells[0]->Value);
	if (folio->Equals("")) {
		button3->Enabled = false;

	}
	else {
		button3->Enabled = true;
	}
}
private: System::Void textBox5_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	button3->Enabled = false;
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		BuscaNombre();
	}
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	InsertPeaton();
	button3->Enabled = false;
	for (int i = 0; i < dataGridView2->Rows->Count; i++) {
		dataGridView2->Rows->Remove(dataGridView2->CurrentRow);
	}
}
private: System::Void InsertPeaton() {

	int id = int::Parse(folio);

	Conexion con = Conexion();
	con.SalidaPeaton(id);


}
private: System::Void dataGridView2_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
};
}


