#pragma once
#include "Conexion.h"
#include "Validaciones.h"

namespace Formulario {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;

	
	/// <summary>
	/// Resumen de VehiculoForm
	/// </summary>
	public ref class VehiculoForm : public System::Windows::Forms::Form
	{
	public:
		VehiculoForm(MySql::Data::MySqlClient::MySqlConnection^ conn)
		{
			InitializeComponent();
			this->conn = conn;
			mostrar();
			llenarCombo();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~VehiculoForm()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;

	private: System::Windows::Forms::TextBox^  textBoxPlaca;
	private: System::Windows::Forms::TextBox^  textBoxMarca;
	private: System::Windows::Forms::TextBox^  textBoxColor;
	private: System::Windows::Forms::TextBox^  textBoxTipo;

	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::BindingSource^  bindingSource1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::ComboBox^  comboBoxBusqueda;

	private: System::Windows::Forms::TextBox^  textBoxBusqueda;

	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::TextBox^  textBoxID;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: MySql::Data::MySqlClient::MySqlConnection^ conn;
	private: System::Windows::Forms::ComboBox^  comboBox1;


	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(VehiculoForm::typeid));
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->textBoxPlaca = (gcnew System::Windows::Forms::TextBox());
			this->textBoxMarca = (gcnew System::Windows::Forms::TextBox());
			this->textBoxColor = (gcnew System::Windows::Forms::TextBox());
			this->textBoxTipo = (gcnew System::Windows::Forms::TextBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->bindingSource1 = (gcnew System::Windows::Forms::BindingSource(this->components));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->comboBoxBusqueda = (gcnew System::Windows::Forms::ComboBox());
			this->textBoxBusqueda = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->textBoxID = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->BeginInit();
			this->SuspendLayout();
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(30, 113);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(52, 16);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Placa:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(30, 147);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(55, 16);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Marca:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(30, 177);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(49, 16);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Color:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(30, 210);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(44, 16);
			this->label5->TabIndex = 4;
			this->label5->Text = L"Tipo:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(30, 248);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(75, 16);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Matricula:";
			// 
			// textBoxPlaca
			// 
			this->textBoxPlaca->Location = System::Drawing::Point(113, 113);
			this->textBoxPlaca->MaxLength = 10;
			this->textBoxPlaca->Name = L"textBoxPlaca";
			this->textBoxPlaca->Size = System::Drawing::Size(145, 20);
			this->textBoxPlaca->TabIndex = 7;
			this->textBoxPlaca->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &VehiculoForm::textBoxPlaca_KeyPress);
			// 
			// textBoxMarca
			// 
			this->textBoxMarca->Location = System::Drawing::Point(113, 147);
			this->textBoxMarca->MaxLength = 30;
			this->textBoxMarca->Name = L"textBoxMarca";
			this->textBoxMarca->Size = System::Drawing::Size(145, 20);
			this->textBoxMarca->TabIndex = 8;
			this->textBoxMarca->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &VehiculoForm::textBoxMarca_KeyPress);
			// 
			// textBoxColor
			// 
			this->textBoxColor->Location = System::Drawing::Point(113, 177);
			this->textBoxColor->MaxLength = 20;
			this->textBoxColor->Name = L"textBoxColor";
			this->textBoxColor->Size = System::Drawing::Size(145, 20);
			this->textBoxColor->TabIndex = 9;
			this->textBoxColor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &VehiculoForm::textBoxColor_KeyPress);
			// 
			// textBoxTipo
			// 
			this->textBoxTipo->Location = System::Drawing::Point(113, 210);
			this->textBoxTipo->MaxLength = 30;
			this->textBoxTipo->Name = L"textBoxTipo";
			this->textBoxTipo->Size = System::Drawing::Size(145, 20);
			this->textBoxTipo->TabIndex = 10;
			this->textBoxTipo->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &VehiculoForm::textBoxTipo_KeyPress);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->BackgroundColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(280, 88);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->Size = System::Drawing::Size(526, 179);
			this->dataGridView1->TabIndex = 12;
			this->dataGridView1->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &VehiculoForm::dataGridView1_CellClick);
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button1->Location = System::Drawing::Point(445, 284);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(108, 38);
			this->button1->TabIndex = 13;
			this->button1->Text = L"Agregar";
			this->button1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &VehiculoForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button2->Location = System::Drawing::Point(686, 284);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(111, 38);
			this->button2->TabIndex = 14;
			this->button2->Text = L"Actualizar";
			this->button2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &VehiculoForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
			this->button3->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button3->Location = System::Drawing::Point(571, 284);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(109, 38);
			this->button3->TabIndex = 15;
			this->button3->Text = L"Eliminar";
			this->button3->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &VehiculoForm::button3_Click);
			// 
			// comboBoxBusqueda
			// 
			this->comboBoxBusqueda->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->comboBoxBusqueda->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxBusqueda->FormattingEnabled = true;
			this->comboBoxBusqueda->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"placa", L"marca", L"matricula" });
			this->comboBoxBusqueda->Location = System::Drawing::Point(394, 47);
			this->comboBoxBusqueda->Name = L"comboBoxBusqueda";
			this->comboBoxBusqueda->Size = System::Drawing::Size(121, 21);
			this->comboBoxBusqueda->TabIndex = 16;
			// 
			// textBoxBusqueda
			// 
			this->textBoxBusqueda->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->textBoxBusqueda->Location = System::Drawing::Point(521, 47);
			this->textBoxBusqueda->Name = L"textBoxBusqueda";
			this->textBoxBusqueda->Size = System::Drawing::Size(159, 20);
			this->textBoxBusqueda->TabIndex = 17;
			this->textBoxBusqueda->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &VehiculoForm::textBoxBusqueda_KeyPress);
			// 
			// button4
			// 
			this->button4->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button4.Image")));
			this->button4->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button4->Location = System::Drawing::Point(706, 37);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(87, 39);
			this->button4->TabIndex = 18;
			this->button4->Text = L"Buscar";
			this->button4->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &VehiculoForm::button4_Click_1);
			// 
			// label7
			// 
			this->label7->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(312, 50);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(76, 13);
			this->label7->TabIndex = 19;
			this->label7->Text = L"Buscar por: ";
			// 
			// textBoxID
			// 
			this->textBoxID->Enabled = false;
			this->textBoxID->Location = System::Drawing::Point(113, 86);
			this->textBoxID->Name = L"textBoxID";
			this->textBoxID->Size = System::Drawing::Size(145, 20);
			this->textBoxID->TabIndex = 20;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(39, 89);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(24, 13);
			this->label1->TabIndex = 21;
			this->label1->Text = L"ID:";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(0, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(35, 13);
			this->label8->TabIndex = 22;
			this->label8->Text = L"label8";
			// 
			// label9
			// 
			this->label9->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			this->label9->Location = System::Drawing::Point(264, 8);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(339, 29);
			this->label9->TabIndex = 23;
			this->label9->Text = L"Administracion de vehiculos";
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(113, 243);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(145, 21);
			this->comboBox1->TabIndex = 24;
			this->comboBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &VehiculoForm::comboBox1_KeyPress);
			// 
			// VehiculoForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(836, 335);
			this->Controls->Add(this->comboBox1);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBoxID);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->textBoxBusqueda);
			this->Controls->Add(this->comboBoxBusqueda);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->textBoxTipo);
			this->Controls->Add(this->textBoxColor);
			this->Controls->Add(this->textBoxMarca);
			this->Controls->Add(this->textBoxPlaca);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Name = L"VehiculoForm";
			this->Text = L"VehiculoForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		//Boton agregar
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {		
		validar();
			
	}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	try
	{
		//int id = Int32::Parse(textBoxID->Text);
	
		String^placa = textBoxPlaca->Text;

		String^ constr = "Server=127.0.0.1;Uid=root;Pwd=raul;Database=estacionamiento";
		MySqlConnection^ con = gcnew MySqlConnection(constr);
		MySqlCommand^ cmd = gcnew MySqlCommand("select * from vehiculo WHERE placa='" +placa + "'", con);
		con->Open();
		MySqlDataReader^ dr = cmd->ExecuteReader();

		while (dr->Read())
		{
			//textBoxID->Text = dr->GetString(0);
			textBoxPlaca->Text = dr->GetString(1);
			textBoxMarca->Text = dr->GetString(2);
			textBoxColor->Text = dr->GetString(3);
			textBoxTipo->Text = dr->GetString(4);
		
		}
		con->Close();
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}

		 //Mostrar
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {	
	MySql::Data::MySqlClient::MySqlDataAdapter^ sda = gcnew MySql::Data::MySqlClient::MySqlDataAdapter("call seleccionarVehiculo()", conn);
	DataTable^ dt = gcnew DataTable();
	sda->Fill(dt);
	dataGridView1->DataSource = dt;

}

		 //Boton ELiminar
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	if (textBoxPlaca->Text == "")
	{
		MessageBox::Show("Por favor seleccione un dato en la tabla para eliminar");
	}
	else
	{
		Eliminar();
	}
	
}
		 //Boton Actualizar
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	int id = Convert::ToInt16(textBoxID->Text);
	String^ placa = textBoxPlaca->Text;
	String^ marca = textBoxMarca->Text;
	String^ color = textBoxColor->Text;
	String^ tipo = textBoxTipo->Text;
	String^ matricula = comboBox1->SelectedItem->ToString();

	Conexion::ActualizarVehiculo(id,placa,marca,color,tipo,matricula,this->conn);

	textBoxID->Text = "";
	textBoxPlaca->Text = "";
	textBoxMarca->Text = "";
	textBoxColor->Text = "";
	textBoxTipo->Text = "";
	//textBoxMatricula->Text = "";
	mostrar();
}
private: System::Void dataGridView1_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	int indexRow = e->RowIndex >= 0 ? e->RowIndex : 0;
	DataGridViewRow^ rowSelected = dataGridView1->Rows[indexRow];
	textBoxID->Text = Convert::ToString(rowSelected->Cells[0]->Value);
	textBoxPlaca->Text = Convert::ToString(rowSelected->Cells[1]->Value);
	textBoxMarca->Text = Convert::ToString(rowSelected->Cells[2]->Value);
	textBoxColor->Text = Convert::ToString(rowSelected->Cells[3]->Value);
	textBoxTipo->Text = Convert::ToString(rowSelected->Cells[4]->Value);
	//textBoxMatricula->Text= Convert::ToString(rowSelected->Cells[5]->Value);
	comboBox1->Text = Convert::ToString(rowSelected->Cells[5]->Value);

}
private: System::Void textBoxMatricula_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validar();
	}

}
		 

				  private:System::Void Eliminar()
				  {
					  String^ placa = textBoxPlaca->Text;
					  int id = Convert::ToInt16(textBoxID->Text);
					  Conexion::EliminarVehiculo(id, this->conn);

					  textBoxID->Text = "";
					  textBoxPlaca->Text = "";
					  textBoxMarca->Text = "";
					  textBoxColor->Text = "";
					  textBoxTipo->Text = "";
					  comboBox1->Text == "";
					  mostrar();
				  }
				  private:System::Void mostrar()
				  {
					  MySql::Data::MySqlClient::MySqlDataAdapter^ sda = gcnew MySql::Data::MySqlClient::MySqlDataAdapter("call seleccionarVehiculo()", conn);
					  DataTable^ dt = gcnew DataTable();
					  sda->Fill(dt);
					  dataGridView1->DataSource = dt;
				  }


private: System::Void textBoxMarca_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validar();
	}
}
private: System::Void textBoxColor_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validar();
	}
}
private: System::Void textBoxTipo_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if(e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validar();
	}
}


	private: System::Void BuscaVehiculos() {
		
		String^ busc = textBoxBusqueda->Text;
		DataTable^ dt = gcnew DataTable();
		MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand("", conn);
		cmd->CommandType = System::Data::CommandType::StoredProcedure;

		switch (comboBoxBusqueda->SelectedIndex)
		{
		case 0:
			cmd->CommandText = "buscarVehiculoPlaca";
			cmd->Parameters->AddWithValue("@iplaca", busc);

			break;
		case 1:
			cmd->CommandText = "buscarVehiculoMarca";
			cmd->Parameters->AddWithValue("@imarca", busc);
			break;
		case 2:
			cmd->CommandText = "buscarVehiculoMatricula";
			cmd->Parameters->AddWithValue("@imatricula", busc);
			break;
		}
		MySql::Data::MySqlClient::MySqlDataReader^ myReader = cmd->ExecuteReader();
		//DataTable^ dt = gcnew DataTable();
		dt->Load(myReader);
		dataGridView1->DataSource = dt;
		myReader->Close();
	}
private: System::Void button4_Click_1(System::Object^  sender, System::EventArgs^  e) {
	validarBusqueda();
}
private: System::Void textBoxBusqueda_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validarBusqueda();
	}
}
		 private: System::Void guardar()
		 {
			 String^ placa = textBoxPlaca->Text;
			 String^ marca = textBoxMarca->Text;
			 String^ color = textBoxColor->Text;
			 String^ tipo = textBoxTipo->Text;
			 String^ matricula = comboBox1->SelectedItem->ToString(); //textBoxMatricula->Text;
			 if (Validaciones::validarPlaca(placa) == false)
			 {
				 try {


					 Conexion::insertarVehiculo(placa, marca, color, tipo, matricula,this->conn);


					 textBoxPlaca->Text = "";
					 textBoxMarca->Text = "";
					 textBoxColor->Text = "";
					 textBoxTipo->Text = "";
					 comboBox1->Text == "";

					 mostrar();
				 }
				 catch (Exception^ ex)
				 {
					 MessageBox::Show("Error en funcion guardar()->"+ex->Message);
				 }
			 }
			 else
			 {
				 MessageBox::Show("La placa no debe comenzar con 0");
			 }
		 }
private: System::Void validar()
{
	if (textBoxPlaca->Text == "")
	{
		textBoxPlaca->Focus();
		MessageBox::Show("Rellene el campo placa por favor");
		return;
	}
	if (textBoxMarca->Text == "")
	{
		textBoxMarca->Focus();
		MessageBox::Show("Rellene el campo Marca por favor");
		return;
	}
	if (textBoxColor->Text == "")
	{
		textBoxColor ->Focus();
		MessageBox::Show("Rellene el campo color por favor");
		return;
	}
	if (textBoxTipo->Text == "")
	{
		textBoxTipo->Focus();
		MessageBox::Show("Rellene el campo tipo por favor");
		return;
	}
	if (comboBox1->SelectedItem->ToString() == "")
	{
		MessageBox::Show("Seleccione una matricula ");
		return;
	}
	
	
	guardar();
}
		private:System::Void validarBusqueda()
		{
			if (textBoxBusqueda->Text == "")
			{
				textBoxBusqueda->Focus();
				MessageBox::Show("No ha ingresado busqueda");
				return;
			}
			BuscaVehiculos();
		}
private: System::Void textBoxPlaca_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validar();
	}
}
		 private: System::Void llenarCombo()
		 {
			 MySql::Data::MySqlClient::MySqlCommand^ cmd = gcnew MySql::Data::MySqlClient::MySqlCommand("matriculaUsuario", conn);
			 cmd->CommandType = System::Data::CommandType::StoredProcedure;
			 MySql::Data::MySqlClient::MySqlDataReader^ myReader = cmd->ExecuteReader();
			
			 while (myReader->Read())
			 {
				 comboBox1->Refresh();
				 comboBox1->Items->Add(myReader->GetValue(0)->ToString());
			 }
			 myReader->Close();
		 }
private: System::Void comboBox1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {

	if (e->KeyChar == Convert::ToChar(System::Windows::Forms::Keys::Enter))
	{
		e->Handled = true;
		validar();
	}
}
};
}
